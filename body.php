<?php
/**
 *   karibu.com
 * * @author Ephraim Swilla <swillae1@gmail.com>
 */
?>
<body> 
    <span id="ajax_setup" style="z-index: 1200;"></span>
    <?php
    $page = '';
    if (isset($_GET['pg']) && $_GET['pg'] != '') {
	$page = $_GET['pg'];

	$url = input::url();

	if (file_exists($url)) {
	    include($url);
	} else {
	    include('404.php');
	}
    } else {

	if (isset($ses_user)) {
	    check_user_status();
	    include('modules/home/home.php');
	} else {
	    include('modules/login/login.php');
	}
    }
    ?>
</body>
