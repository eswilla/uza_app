/* 
 Document   : alert
 Created on : 20-april-2013, 01:27:00
 Author     : Ephraim Swilla <swillae@yahoo.com>
 Description:
 Purpose of this js is to customize user interface.
 */
$.ajaxSetup({
    timeout: 20000,
    beforeSend: function() {
     $('#ajax_setup').html(LOADER);
    },
    complete: function() {
       $('#ajax_setup').html('');
    }
});

get_send = function(param, return_div) {
    $('#' + return_div).html(LOADER);
    $.get(url, param, function(data) {
        $('#' + return_div).html(data);
    });
};
push_url=function(pageurl) {
    //to change the browser URL to 'pageurl'
    if (typeof (pageurl) === 'undefined')
        pageurl = '';
    else
        document.title = 'karibuSMS ' + pageurl;
    pageurl = home + pageurl;

    if (!$.browser.msie) {
        if (pageurl != window.location) {
            window.history.pushState({path: pageurl}, '', pageurl);
        }
    }
    return false;
}
site_search = function() {
    $('#searchbox').keyup(function() {
        var searchbox = $(this).val();
        if (searchbox == '') {
            $('#sechdiv').hide();

        } else {
            //$('#ajax_site_search_result').html(LOADER);
            $.get(url, {
                pg: "general",
                process: "search/search",
                searchword: searchbox
            }, function(data) {
                $('#sechdiv').html('');
                $('#sechdiv').show().html(data);
            });
        }
    }).blur(function() {
        if (searchbox == '') {
            $('#sechdiv').hide();

        }
    });
}
$(document).ready(site_search);