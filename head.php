<?php
/**
 * * @author Ephraim Swilla <swillae1@gmail.com> 
 */
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="karibu, SMS web app, SMS business support, people get connected with business, Customer support application, be the first to know" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
    <meta charset="utf-8"/>
    <link href="media/images/logo.png" rel="shortcut icon" type="image/x-icon" />
    <meta name="google-translate-customization" content="f08fefcb14c1447d-a1f03246bc4214cf-gecf7d7142d3c86be-11"></meta>
   <!-- <script language="JavaScript" src="http://www.geoplugin.net/javascript.gp" type="text/javascript"></script> -->
    <?php
    /* --------all css media and js load here------- */
    ?>
    <script type="text/javascript">
        //----------------JavaScript variables------------------------------ 

//        var country = {
//            name: geoplugin_countryName(),
//            region: geoplugin_region(),
//            city: geoplugin_city(),
//            code: geoplugin_countryCode(),
//            continent: geoplugin_continentCode(),
//            latitude: geoplugin_latitude(),
//            longitude: geoplugin_longitude(),
//            currency_code: geoplugin_currencyCode(),
//            currency_symbol: geoplugin_currencySymbol()
//        };
        var url = "<?= $AJAX ?>";
        var home = "<?= HOME ?>";
        var LOADER = '<?= $LOADER ?>';
    </script> 
    <?php
    css_media('app.v2');
    css_media('font');
    //css_media('landing');
    ?>
    <!--[if lt IE 9]> 
    <script src="js/ie/html5shiv.js"></script> 
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/excanvas.js"></script> 
    <![endif]-->
    <?php
    js_media('app.v2');
    js_media('custom');
   // js_media('jquery.jcryption.3.0.1');

    ?>
    <title><?=  isset($ses_user)? $ses_user->name: TITLE ?></title>   
</head>