<!-- header --> 
<header id="header" class="navbar navbar-fixed-top bg-white box-shadow b-b b-light" data-spy="affix" data-offset-top="1"> <div class="container">
        <div class="navbar-header">
            <a href="<?= HOME ?>" class="navbar-brand">
                <img src="media/images/logo.png" class="m-r-sm">
                <span class="text-muted">karibuSMS</span></a> 
            <button class="btn btn-link visible-xs" type="button" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li> <a href="<?= HOME ?>features">Features</a> </li> 
                <li> 
                    <a href="<?= HOME ?>payment">Pricing</a> 
                </li>
                <li> <div class="m-t-sm">
                        <a href="#" class="btn btn-link btn-sm">Sign in</a>
                        <a href="<?= HOME ?>register" class="btn btn-link btn-sm"><strong>Sign up</strong></a></div> </li>
            </ul> </div> </div>
</header> <!-- / header -->

<section id="content"> 

    <div class="bg-gradient"> 

        <div class="text-center wrapper"> 
            <div class="h4 text-muted m-t-sm">
               karibuSMS web application</div>
            <a href="<?= HOME ?>register" class="btn btn-lg btn-primary b-primary  m-sm">Sign Up</a><br/>
            OR
        </div> 

        <div class="padder"> 
            <div class="hbox"> 
                <section id="content" class="m-t-lg wrapper-md animated fadeInUp">

                    <div class="container aside-xxl"> 
                        <section class="panel panel-default bg-white m-t-lg"> 
                            <header class="panel-heading text-center">
                                <strong>Sign in</strong> 
                            </header> 
                            <form class="panel-body wrapper-lg" id="signin_form"> 
                                <div class="form-group"> 
                                    <label class="control-label">Phone number</label> 
                                    <input type="text" name="phone_number" id="phone" required="required" placeholder="0XXX XYZ XYZ" class="form-control input-lg"> 
                                </div> 
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input type="password" name="password" id="inputPassword" required="required" placeholder="Password" class="form-control input-lg">
                                </div> 
                                <div class="checkbox"> 
                                    <label> <input type="radio" name="service" checked="checked" value="karibuSMS"> Sign in to karibuSMS </label> 
                                    <label><BR/> <input type="radio" name="service" checked="checked" value="pesaSMS"> Sign in to PesaSMS </label> 
                                </div> 
                                <div class="checkbox"> 
                                    <label> <input type="checkbox" name="checkbox" checked="checked"> Keep me sign in </label> 
                                </div> 
                                <a href="<?= HOME ?>login&sec=forget_pw" class="pull-right m-t-xs"><small>Forgot password?</small></a> 
                                <button type="button" class="btn btn-primary" onclick="javascript: signin();">Sign in</button> 
                                <div id="login_ajax_request"></div>
                                <div class="line line-dashed"></div>	  
                            </form> 

                        </section> 
                    </div>

                </section> 
            </div>
        </div>

        <div class="dker pos-rlt">
            <div class="container wrapper">
                <div class="m-t-lg m-b-lg text-center">
                    SMS solution for businesses,organizations and any individual </div>
            </div> 
        </div> 
    </div> 

    <div id="about">
        <div class="container">
            <div class="m-t-xl m-b-xl text-center wrapper">
                <h3>What is karibuSMS ?</h3> 
                <p class="text-muted">
                    Comes from SWAHILI word karibu (which means welcome or come close). <br/>karibuSMS is Is the advertising and promotion, SMS platform, that keeps businesses connected to their customers via SMS and with online profile.</p> 
            
                    <h3>Basic Features</h3>
            </div> 
            <div class="row m-t-xl m-b-xl text-center">
                
                <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
                    <p class="h3 m-b-lg"> 
                        <i class="fa fa-mobile fa-3x text-info"></i> 
                    </p> 
                    <div class="">
                        <h4 class="m-t-none">Mobile Application</h4>
                        <p class="text-muted m-t-lg">karibuSMS can work by utilizing normal SMS in android smartphone after install karibuSMS in your phone <a href="http://goo.gl/msamgD" target="_blank" class="label label-info"> here</a> or just use <a class="label label-info" href="<?= HOME ?>karibusmspro">internet messages</a>  to send SMS that goes with your business brand name at the top of SMS instead of phone number.</p> </div> 
                </div>
                <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="600"> <p class="h3 m-b-lg"> <i class="fa fa-user fa-3x text-info"></i> </p> <div class=""> <h4 class="m-t-none">Online profile</h4> <p class="text-muted m-t-lg">For any business account created, a business profile is created which allow you to pinpoint business map location, upload product photos , videos and much more.</p> </div> </div> 
                <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="900"> <p class="h3 m-b-lg"> <i class="fa fa-gears fa-3x text-info"></i> </p> <div class=""> 
                        <h4 class="m-t-none">Developer API</h4> 
                        <p class="text-muted m-t-lg">karibuSMS <a href="<?= HOME ?>api" class="label label-info" title="PHP and jQuery APIs"> APIs</a> allows different systems (software) to be integrated with SMS solution. Whether you have a web application, desktop application or any application that needs SMS solution, you can easily integrate with karibuSMS to allow SMS sending.</p> </div> </div> 
            </div> 
        </div> </div>


    <div id="responsive" class="bg-dark">
        <div class="text-center">
            <div class="container"> 
                <div class="m-t-xl m-b-xl wrapper"> 
                    <h3 class="text-white">Connect your online profile with social networks</h3> 
                    <p>Once you are in karibuSMS, you are in other social network also. This allows you to be connected with other networks and share one message in all platforms </p> 
                    <br>KaribuSMS is now connected with</div>
                <div class="row m-t-xl m-b-xl"> 
                    <div class="col-sm-4 wrapper-xl" data-ride="animated" data-animation="fadeInLeft" data-delay="300"> <p class="text-center h2 m-b-lg m-t-lg"> <span class="fa-stack fa-2x">
                                <i class="fa fa-facebook-square fa-stack-2x text-dark"></i> 
                                <i class="fa fa-facebook fa-stack-1x text-muted"></i> </span> </p> 
                        <p>Facebook</p> 
                    </div> 
                    <div class="col-sm-4 wrapper-xl" data-ride="animated" data-animation="fadeInLeft" data-delay="600"> <p class="text-center h1 m-b-lg"> 
                            <span class="fa-stack fa-2x"> 
                                <i class="fa fa-twitter-square fa-stack-2x text-dark"></i> 
                                <i class="fa fa-twitter fa-stack-1x text-muted"></i> </span> 
                        </p> <p>Twitter</p> 
                    </div>
                    <div class="col-sm-4 wrapper-xl" data-ride="animated" data-animation="fadeInLeft" data-delay="900"> <p class="text-center h2 m-b-lg m-t-lg"> <span class="fa-stack fa-2x"> 
                                <i class="fa fa-google-plus-square fa-stack-2x text-dark"></i> 
                                <i class="fa fa-google-plus fa-stack-1x text-muted text-md"></i> </span> </p> <p>Google+</p> </div> </div> </div> </div> </div> 

    <div id="newsletter" class="bg-white clearfix wrapper-lg"> <div class="container text-center m-t-xl m-b-xl" data-ride="animated" data-animation="fadeIn">
            <h2>Like or follow us on social networks</h2> 
            <p>We are social, and we like to hear from you and be friends</p> 
            <div class="m-t-sm m-b-lg"> 

                <p> <a  title="twitter" href="https://twitter.com/karibuSMS" class="twitter-follow-button" data-show-count="true" data-lang="en">Follow @karibuSMS</a>

                    <a href="#" title="Facebook">
                        <div class="fb-like" data-href="http://karibusms.com" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>

                    </a> 
                    <!-- Place this tag in your head or just before your close body tag. -->
                    <script src="https://apis.google.com/js/platform.js" async defer></script>
                    <link rel="canonical" href="http://www.karibusms.com" />
                    <!-- Place this tag where you want the +1 button to render. -->
                <div class="g-plusone" data-annotation="inline" data-width="300"></div>
                </p>
            </div>
        </div> </div>
</section> <!-- footer --> 

<footer id="footer"> <div class="bg-primary text-center"> <div class="container wrapper"> <div class="m-t-xl m-b"> For reliable SMS solution <a href="<?= HOME ?>register"  class="btn btn-lg btn-dark b-white bg-empty m-sm">Sign Up</a> or <a href="#" class="btn btn-lg btn-info b-white bg-empty m-sm">Sign in</a> </div> </div> <i class="fa fa-caret-down fa-4x text-primary m-b-n-lg block"></i> </div> <div class="bg-dark dker wrapper"> 
    </div> </footer> <!-- / footer -->
<?php
css_media('landing');
?>
<script>
                                    $('body').keypress(function(e) {
                                        if (e.which == 13) {
                                            signin();
                                        }
                                    });
                                    signin = function() {
                                        if ($('#inputPassword').val == '' || $('#phone').val() == '') {
                                            $('#login_ajax_request').html('<br/><div class="alert alert-danger">\n\
                                   <button type="button" class="close" data-dismiss="alert">×</button>\n\
                                   <i class="fa fa-ban-circle"></i> Fill all fields</div>');
                                            return false;
                                        }
                                        var datastring = $('#signin_form').serialize();
                                        $('#login_ajax_request').html(LOADER);
                                        $.getJSON(url, {pg: 'login', process: 'login', datastring: datastring}, function(data) {
                                            if (data.error == 0) {
                                                window.location.reload();
                                            } else {
                                                $('#login_ajax_request').html(data.error);
                                            }
                                        }).error(function(data) {
                                            console.log(data);
                                        });
                                    };
                                    function call_validate() {
                                        var phone_number = $('#phone').val();
                                        $('#content').html(LOADER);
                                        $.get(url, {pg: 'register', section: 'validate_phonenumber', phone_number: phone_number}, function(data) {
                                            $('#content').html(data);
                                        });
                                    }
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=526011487520671&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = "//platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, "script", "twitter-wjs");</script>