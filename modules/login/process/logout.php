<?php

/**
 * @author Ephraim Swilla <swillae1@gmail.com>
 * 
 */
// Force script errors and warnings to show on page in case php.ini file is set to not display them
error_reporting(E_ALL);
ini_set('display_errors', '1');
// Unset all of the session variables

$_SESSION = array();
// If it's desired to kill the session, also delete the session cookie
// Destroy the session variables


//destroy cookie also
setcookie(PHONE_COOKIE, $input->encrypt($ses_user->phone_number), time() - 60 * 60 * 24 * 100, "/");
setcookie(PASS_COOKIE, $input->encrypt($ses_user->password), time() - 60 * 60 * 24 * 100, "/");


session_destroy();

redirect_to(HOME);
?> 