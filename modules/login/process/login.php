<?php

/**
 * @author Ephraim Swilla <swillae1@gmail.com>
 */
class log {

    public $email;
    public $password;
    public $error;
    private $checked;
    private $service;
    private $user;

    public function __construct() {
        if (!empty($_GET)) {
            $data = array();
            parse_str($_GET['datastring'], $data);
            $this->email = $data['phone_number'];
            $this->password = $data['password'];
            $this->service=$data['service'];
            $this->checked = isset($data['checkbox'])? $data['checkbox']:'';
            $this->log_user_in();
        }
    }

    public function error($error) {
        $this->error = $error;
        $id = $_GET['posted_id'];
        if (empty($id)) {
            $id = 0;
        } else {
            $id = $id++;
        }
        $id++;
    }

    public function log_user_in() {

        $password = sha1(md5(sha1(($this->password))));

        $number = validate_phone_number($this->email);
        $data = array('phone_number' => $number[1], 'password' => $password);
        $user_info = business::find_where($data);
        $this->user = array_shift($user_info);
        
        if($this->service=='pesaSMS'){
            $_SESSION['pesasms']=TRUE;
        }
        if (!empty($this->user)) {
           // if ($this->user->phone_number_valid == 1) {
                $this->set_session_cookie();
                if (isset($_SESSION['id'])) {
                    echo json_encode(array(
                        'error' => 0,
                        'success' => 'Log in successful'
                    ));
                } else {
                    echo json_encode(array(
                        'error' => 1,
                        'success' => 'Log in successful'
                    ));
                }
//            } else {
//                echo json_encode(array(
//                    'error' => '<br/><div class="alert alert-info">'
//                    . ' <button type="button" class="close" data-dismiss="alert">×</button> '
//                    . '<i class="fa fa-info-sign"></i><strong>Soory!</strong>'
//                    . ' This number is not validated <br/>'
//                    . '<a href="#" onclick="return false" onmousedown="call_validate()">Click here to enter validation code</a></div> '
//                ));
//            }
        } else {
            echo json_encode(array(
                'error' => '<br/><div class="alert alert-danger"> 
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-ban-circle"></i> 
            <a href="#" class="alert-link">Wrong phone number or password supplied</a>.
            </div>'
            ));
        }
    }

    private function set_session_cookie() {
        global $input;
        $id = $this->user->id;
        $userpass = $this->user->password;
        $_SESSION['id'] = $id;
        $_SESSION['username'] = $this->user->username;
        $_SESSION['idx'] = base64_encode("g4p3h9xfn8sq03hs2234$id"); //use of token when nessessary
        if ($this->checked == 'on') {
        setcookie(PHONE_COOKIE, $input->encrypt($this->user->phone_number), time() + 60 * 60 * 24 * 100, "/"); // Cookie set to expire in about 30 days
        setcookie(PASS_COOKIE, $input->encrypt($this->user->password), time() + 60 * 60 * 24 * 100, "/"); // Cookie set to expire in about 30 days

        }
    }
}

if (!empty($_GET)) {

    $login = new log();
}
?>