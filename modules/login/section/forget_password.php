<?php
/**
 *   karibu.com
 * * @author Ephraim Swilla <swillae1@gmail.com>
 */
?>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">

    <div class="container aside-xxl"> 
        <a class="navbar-brand block" href="<?= HOME ?>">Karibu..!</a> 
        <section class="panel panel-default bg-white m-t-lg"> 
            <header class="panel-heading text-center">
                <strong>Forget password</strong> 
            </header> 
            <form class="panel-body wrapper-lg" id="signin_form"> 
                <div class="form-group" id="no"> 
                    <label class="control-label">Your phone number</label> 
                    <input type="text" name="phone_number" id="phone_number" required="required" placeholder="e.g 0714 011 022" class="form-control input-lg"> 
                </div> 

                <a href="<?= HOME ?>" class="pull-right m-t-xs"><small id="s">Suddenly remembered?</small></a> 
                <button type="button" class="btn btn-primary" onclick="javascript: forget();">Submit</button> 
                <div id="fg_ajax_request"></div>
                <div class="line line-dashed"></div>
                <p class="text-muted text-center"><small>Do not have an account?</small></p> 
                <a href="<?= HOME ?>register" class="btn btn-default btn-block">Create an account</a>
            </form> 
        </section> 
    </div>
</section> 
<?php include_once 'modules/landing/footer.php'; ?>
<script>
                    forget = function() {
                        var phone = $('#phone_number').val();
                        if (phone == '') {
                            $('#fg_ajax_request').html('<br/><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><i class="fa fa-ban-circle"></i> Fill your phone number</div>');
                            return false;
                        }
                        $.getJSON(url, {pg: 'login', process: 'forget_password', phone_number: phone}, function(data) {
                            if (data.error != '') {
                                $('#fg_ajax_request').html(data.error);
                            } else {
                                $('#signin_form').html(data.success);
                            }
                        }).error(function(data) {
                            console.log(data);
                        });
                    };
</script>