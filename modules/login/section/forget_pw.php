<?php
/**
 *   karibu.com
 * * @author Ephraim Swilla <swillae1@gmail.com>
 */
?>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">

    <div class="container aside-xxl"> 
        <a class="navbar-brand block text-success" href="<?= HOME ?>">KaribuSMS</a> 
        <section class="panel panel-default bg-white m-t-lg"> 
            <header class="panel-heading text-center">
                <strong>Forgot password</strong> 
            </header> 
            <form class="panel-body wrapper-lg" id="signin_form"> 
                <div class="form-group" id="no"> 
                    <label class="control-label">Your phone number</label> 
                    <input type="text" name="phone_number" id="phone_number" required="required" placeholder="0XYZ XYZ XYZ" class="form-control input-lg"> 
                </div> 
                <div class="line line-dashed line-lg pull-in"></div>
                <div class="form-group"> 
                    <label class="col-sm-2 control-label">Calculate</label> 
                    <div class="col-sm-10"> 
                        <div class="row"> 
                            <div class="col-md-2"><b class="col-lg-12" id="first_no"></b></div>
                            <div class="col-md-2">+<b id="second_no" class="col-lg-12"></b></div>
                            <div class="col-md-4"> <input type="text" id="validate_value" class="form-control input-sm"> </div>
                        </div> 
                    </div> 
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
                <a href="<?= HOME ?>" class="pull-right m-t-xs"><small id="s" data-toggle="tooltip" data-placement="bottom" data-original-title="Click to go to sign in page">Suddenly remembered?</small></a> 
                <button type="button" class="btn btn-primary" onclick="javascript: forget();">Submit</button> 
                <div id="fg_ajax_request"></div>
                
            </form> 
        </section> 
    </div>
</section> 
<?php include_once 'modules/landing/footer.php'; ?>
<script>
    forget = function() {
        //generate random number here

        var phone = $('#phone_number').val();
        var validate_no = $('#validate_value').val();
        var first_no = $('#first_no').html();
        var second_no = $('#second_no').html();

        if (phone == '') {
            $('#fg_ajax_request').html('<br/><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><i class="fa fa-ban-circle"></i> Fill your phone number</div>');
            return false;
        } else if (Math.floor(second_no) + Math.floor(first_no) != validate_no) {
            $('#fg_ajax_request').html('<br/><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><i class="fa fa-ban-circle"></i>Calculation is incorrect</div>');
            calculate();
            return false;
        } else {
            $.getJSON(url, {pg: 'login', process: 'forget_password', phone_number: phone}, function(data) {
                if (data.error != '') {
                    $('#fg_ajax_request').html(data.error);
                } else {
                    $('#signin_form').html(data.success);
                }
            }).error(function(data) {
                console.log(data);
            });
        }
    };
    calculate = function() {
        $('#first_no').html(Math.floor((Math.random() * 10) + 1));
        $('#second_no').html(Math.floor((Math.random() * 10) + 1));
    };
    $(document).ready(calculate);
</script>