 <section class="panel panel-default">
     <div class="carousel slide auto panel-body" id="c-slide">
         <ol class="carousel-indicators out">
             <li data-target="#c-slide" data-slide-to="0" class=""></li> 
             <li data-target="#c-slide" data-slide-to="1" class=""></li>
             <li data-target="#c-slide" data-slide-to="2" class="active"></li>
         </ol> 
         <div class="carousel-inner"> 
             <div class="item active"> 
                 <p class="text-center"> 
                     <em class="h4 text-mute">About</em><br> 
                     <small class="text-muted">A way to be connected with your customers or clients and easily notify about your products or services</small> 
                 </p> 
             </div>
             <div class="item"> 
                 <p class="text-center"> 
                     <em class="h4 text-mute">Get new customers</em><br> 
                     <small class="text-muted">Tell them to send SMS with keyword KARIBU YOURBUSINESSNAME to 15573 and they will be in your list</small> 
                 </p>
             </div>
             <div class="item"> 
                 <p class="text-center"> 
                     <em class="h4 text-mute">Simple to work</em><br>
                     <small class="text-muted">Use either <a style="color: #8ec165" href="<?=HOME?>help&sec=features">web application</a> or <a style="color: #8ec165" href="media/doc/market/karibuSMS_english.pptx">mobile phone</a> interaction to be in touch with your customers or clients</small>
                 </p>
             </div> 
         </div>
         <a class="left carousel-control" href="#c-slide" data-slide="prev"> <i class="fa fa-angle-left"></i> </a>
         <a class="right carousel-control" href="#c-slide" data-slide="next"> <i class="fa fa-angle-right"></i> </a> 
     </div>
 </section> <!-- / .carousel slide -->
                   
