<?php

/**
 *   karibu.com
 * * @author Ephraim Swilla <swillae1@gmail.com>
 */
function error($error_msg) {
    return '<div class="alert alert-danger"> 
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-ban-circle"></i> Error,
            <a href="#" class="alert-link">' . $error_msg . '.</a>
            </div>';
}

if (!empty($_GET)) {

    $data = array();
    parse_str($_GET['datastring'], $data);

    $number = validate_phone_number($data['phone_number']);
    $email = $data['email'];
    if (preg_match('/[^a-zA-Z0-9 ]/i', $data['business_name'])) {
        echo json_encode(
        array(
        'error' => error('Invalid characters found in a name'),
        'success' => ''
        ));
        exit;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo json_encode(
        array(
        'error' => error('This email is not valid'),
        'success' => ''
        ));
        exit;
    }
    if (!is_array($number)) {
        //error, number is not valid
        echo json_encode(
        array(
        'error' => error('Invalid phone number, enter it correctly'),
        'success' => ''
        ));
        exit;
    } else if (strlen($data['password']) < 6) {
        //error, password is too short
        echo json_encode(
        array(
        'error' => error('Password is too short, use at least 6 chars'),
        'success' => ''
        ));
        exit;
    } else {
        $password = sha1(md5(sha1($data['password'])));
        $codes = rand(1, 999999);
        if (strlen($data['business_name']) < 2) {
            echo json_encode(
            array(
            'error' => error('Business name is too short, use at least 3 chars'),
            'success' => ''
            ));
            exit;
        }
        $business_info = business::find_where("phone_number='" . $number[1] . "'");
        if (!empty($business_info)) {
            echo json_encode(
            array(
            'error' => error('This phone number is in use, Log in now with this number'),
            'success' => ''
            ));
            exit;
        }
        $business_name = business::find_where("name='" . $data['business_name'] . "'");
        if (!empty($business_name)) {
            echo json_encode(
            array(
            'error' => error('This business name is in use, use different one'),
            'success' => ''
            ));
            exit;
        }
        //$username=preg_replace('/ /', '', substr($data['business_name'], 0, 10));
        $new_data = array(
        'name' => $data['business_name'],
        'username' => str_replace(' ', '_', $data['business_name']),
        'phone_number' => $number[1],
        'verification_code' => $codes,
        'email' => $email,
        'password' => $password,
        'country' => $number[0],
        );
        //  lets notify ourself about any error occur somewhere
        $headers = 'From: karibuSMS <info@karibusms.com>' . "\r\n";
        $headers .= "X-Mailer: PHP/" . phpversion() . "\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: text/html; charset=iso-8859-1";
        $message = "Hi " . $data['business_name'] . "<br/>"
        . "Welcome in karibuSMS, advertising and promotion, SMS platform, that keeps you connected to your customers          or people you need with SMS notification.<br/><br/>"
        . "To learn about karibuSMS features, visit here <a href='http://karibusms.com/features'>http://karibusms.com/features</a></br/>"
        . "To integrate SMS solution in your softwares or computer systems. visit here <a href='http://karibusms.com/api'>http://karibusms.com/api</a></br/>"
        . "To download our android application, visit here <a href='http://goo.gl/msamgD'>http://goo.gl/msamgD</a></br/>"
        . "For any help or support on how to use the application or benefit from this application, contact us on info@karibusms.com Or Call us via +255 759 55 33 55."
        . "<br/>"
        . "Thanks";
        mail($email, "Welcome in karibuSMS", $message, $headers);
        $result = $db->insert('business', $new_data);
        $business_id = $db->id();
        if ($result) {
            //lets send sms to validate phone number if is in use
             $verification_message = "Verification codes are: " . $codes;
               
            $sms = new sms_sender();
            $sms->set_from_name('KaribuSMS');
            $sms->set_message($verification_message);
            $sms->set_phone_number($number[1]);
            $send = $sms->send();
            if (!$send) {
                $return["posts"] = array();
                $post = array();
                $post["phonenumber"] = $number[1];
                array_push($return["posts"], $post);
                $sms = array("Notice" => $verification_message, "server" => $return);
                $send = gcm::push_from_platform($sms);
            }
            if ($send) {
                ob_start();
                include_once 'modules/register/modal_validate.php';
                $view = ob_get_clean();
                ob_end_flush();
                echo json_encode(array(
                'error' => '',
                'success' => $view
                ));
            } else {
                error_record("error occur in sending sms to business id=" . $business_id, $result, TRUE);
            }
        } else {
            echo json_encode(
            array(
            'error' => error('Error occurs, please try again later'),
            'success' => ''
            ));
        }
    }
}
?>
