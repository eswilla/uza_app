<?php

class verification_code {

    private $codes;
    private $business;

    public function __construct() {
        if (isset($_GET['ver_codes'])) {
            $this->codes = $_GET['ver_codes'];
            if ($this->codes == '') {
                echo 'Enter codes first';
                exit();
            }
            $this->update_records();
        } else {
            $this->get_business();
        }
    }

    public function get_business() {
        $business_info = business::find_by_id($_GET['business_id']);
        $this->business = array_shift($business_info);
        $this->set_session();
    }

    function update_records() {
        global $db;
        $business_info = business::find_where("verification_code='" . $this->codes . "'");
        if (!empty($business_info)) {
            $this->business = array_shift($business_info);

            $data = array(
                'phone_number_valid' => 1
            );
            $db->update('business', $data, array("id" => $this->business->id));
            $this->set_session();
        } else {
            echo 'Codes are invalid, please enter correct codes';
            exit();
        }
    }

    function set_session() {
        global $input;
        $_SESSION['login'] = TRUE;
        $_SESSION['id'] = $this->business->id;
        $_SESSION['idx'] = base64_encode("g4p3h9xfn8sq03hs2234{$this->business->id}"); //use of token when nessessary
        setcookie(PHONE_COOKIE, $input->encrypt($this->business->phone_number), time() + 60 * 60 * 24 * 100, "/"); // Cookie set to expire in about 30 days
        setcookie(PASS_COOKIE, $input->encrypt($this->business->password), time() + 60 * 60 * 24 * 100, "/"); // Cookie set to expire in about 30 days

        $session_user = business::find_by_id($_SESSION['id']);
        $ses_user = array_shift($session_user);
        echo '1';
    }

}

new verification_code();
?>
