<?php

if (!empty($_GET)) {
    $username = $_GET['username'];
    $bad_worlds = array('kikwete', 'mkapa', 'serikali', 'polisi', 'mahakama');
    $error = '<div class="alert alert-danger"> '
            . '<button type="button" class="close" data-dismiss="alert">×</button> '
            . '<i class="fa fa-ban-circle"></i><strong>Error!</strong> '
            . '<a href="#" class="alert-link">';
    $business_info = business::find_where("username='" . $username . "'");
    if (!empty($business_info)) {
        echo $error . 'this username is in use. Try another one</a> </div>';
        exit();
    } else if (strlen($username) < 3) {
        echo $error . ' use at least 3 characters </a> </div>';
        exit();
    } else if (in_array(strtolower($username), $bad_worlds)) {
        echo $error . 'this name ' . $username . ' is not allowed</a> </div>';
        exit();
    } else {
        $db->update('business', array('username' => $username), "id='" . $ses_user->id . "'");
        echo '<div class="alert alert-success">'
        . ' <button type="button" class="close" data-dismiss="alert">×</button> '
        . '<i class="fa fa-ok-sign"></i><strong>Well done!</strong> '
        . 'Username has been taken successful.'
        . ' </div><br/></br/>'
        . '<button class="btn-success" data-dismiss="modal">Click to close</button> ';
    }
}