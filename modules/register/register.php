<?php
/**
 *   karibu.com
 * * @author Ephraim Swilla <swillae1@gmail.com>
 */
?>
<div class="container aside-xxl" id="register">
    <a class="navbar-brand block text-success" href="<?= HOME ?>">KaribuSMS</a>
    <section class="panel panel-default m-t-lg bg-white"> 
        <header class="panel-heading text-center"> <strong>Sign up</strong> </header>
        <form class="panel-body wrapper-lg" id="signup_form"> 
            <div class="form-group"> <label class="control-label">Name</label>
                <input type="text" name="business_name" placeholder="ie. Your business name" class="form-control input-lg">
            </div> 
            <div class="form-group"> 
                <label class="control-label">Email</label> 
                <input type="text" name="email" placeholder="e.g name@example.com" class="form-control input-lg">
            </div>
            <div class="form-group"> 
                <label class="control-label">phone number</label> 
                <input type="text" name="phone_number" placeholder=" XYZ ZZZ XXX" class="form-control input-lg">
            </div> 
            <div class="form-group"> 
                <label class="control-label">password</label> 
                <input type="password" name="password" id="inputPassword" placeholder="password" class="form-control input-lg"> 
            </div> 
            <div class="checkbox"> 
                <label> <input type="checkbox" name="agree_terms_and_conditions"> Agree to the <a href="<?= HOME ?>register&sec=privacy">terms and policy</a> </label> 
            </div> 
            <button type="button" class="btn btn-primary" id="signup">Sign up</button> 
            <div id="ajax_results"></div>
            <div class="line line-dashed"></div> 
            <p class="text-muted text-center"><small>Already have an account?</small></p> 
            <a href="<?= HOME ?>" class="btn btn-default btn-block">Sign in</a>
            <p align="center" class="text-success"><br/><a href="<?= HOME ?>help&sec=features" >Learn about karibuSMS here</a></p>

        </form>
    </section> 
</div>
<?php include_once 'modules/landing/footer.php'; ?>
<script>
    register_business = function() {
        $('#signup').click(function() {
            validate_form();
            var datastring = $('#signup_form').serialize();
            $('#ajax_results').html(LOADER);
            $.getJSON(url, {pg: 'register', process: 'register', datastring: datastring}, function(data) {
                if (data.error != '') {
                    $('#ajax_results').html(data.error).css('margin-top', '5px');
                } else {
                    $('#register').html(data.success).css('margin-top', '5px');
                }
            }).error(function(data) {
                console.log(data);
            });
        });
        function validate_form() {
            $('input', '#signup_form').each(function(e) {
                var value = $(this).val();
                var arr_unchecked_values = $('input[type=checkbox]:not(:checked)').map(function() {
                    return this.value
                }).get();
                if (value == '') {
                    var name = $(this).attr('name');
                    var field = name.replace(/_/g, ' ');
                    $('#ajax_results').html('<br/><div class="alert alert-danger">\n\
                      <button type="button" class="close" data-dismiss="alert">×</button>\n\
                        <i class="fa fa-ban-circle"></i> ' + field + ' is empty</div>');
                    e.stopPropagation();
                } else if (arr_unchecked_values == 'on') {
                    $('#ajax_results').html('<br/><div class="alert alert-danger">\n\
                      <button type="button" class="close" data-dismiss="alert">×</button>\n\
                        <i class="fa fa-ban-circle"></i> You have to agree with terms and conditions first</div>');
                    e.stopPropagation();
                }
            });
        }
    };


    jQuery(document).ready(register_business);
</script>