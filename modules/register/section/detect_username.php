
<section class="hbox stretch">
    <div class="modal-dialog"> 
        <div class="modal-content">    
            <div class="modal-body"> 
                <button type="button" class="close" data-dismiss="modal">&times;</button> 
                <div class="row"> 
                    <div class="col-sm-6 b-r">
                        <h3 class="m-t-none m-b">Important..!</h3> 
                        <p>Add business username here.</p> 
                        
                            <div class="form-group"> 
                                <label>Username</label>
                                <input type="text" id="username_input" class="form-control" placeholder="Enter username" maxlength="11">
                                <small class="label bg-info">Maximum 10 characters with no space</small>
                            </div>

                            <div class="checkbox m-t-lg">
                                <button id="username_submit"  class="btn btn-sm btn-success pull-right text-uc m-t-n-xs">
                                    <strong>Submit</strong>
                                </button> 

                            </div>
                       
                    </div> 
                    <div class="col-sm-6" id="results"> 
                        <h4>Why username?</h4>
                        <p>This is mandatory because </p>
                        <ol>
                            <li>It is uniquely identify your business</li>
                            <li>All your customers will subscribe to this name only</li>
                            <li>SMS will be sent to your customers having this name</li>
                        </ol>  
                    </div> 
                </div>
            </div> 
        </div><!-- /.modal-content --> 
    </div><!-- /.modal-dialog --> 
</section>
<script>
    username_add = function() {
        $('#username_submit').click(function() {
            var name = $('#username_input').val();
            if (name !== '') {
                $('#results').html(LOADER);
                $.get(url, {pg: 'register', process: 'add_username', username: name}, function(data) {
                    $('#results').html(data);
                });
            }
        });
    };
    $(document).ready(username_add);

</script>
