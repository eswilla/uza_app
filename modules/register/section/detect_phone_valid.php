<?php
$phone_number = $_GET['phone_number'];
?>
<section class="hbox stretch">
    <div class="modal-dialog"> 
        <div class="modal-content">    
            <div class="modal-body"> 
                <button type="button" class="close" data-dismiss="modal">&times;</button> 
                <div  class="row"> 
                    <a class="navbar-brand block" href="<?= HOME ?>">KaribuSMS</a>
                    <div class="text-center" style="width:250px; margin: 0 auto;">
                        <div class="thumb-md"><img src="media/images/avatar_default.jpg" class="img-circle b-a b-light b-3x"></div> 
                        <p class="text-white h4 m-t m-b"></p>
                        <p class="text-dark h4 m-t m-b">Enter codes you receive in your phone</p>
                        <div class="input-group"> 
                            <input type="text" id="verification_codes" class="form-control text-sm" placeholder="Enter verification codes"> 
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="button" onclick="return false;"
                                        onmousedown="javascript: validate_this_number();" id="ver_codes">
                                    <i class="fa fa-arrow-right"></i>
                                </button>
                            </span>


                        </div> 
                        <p id="ajax_modal_results" align="center"></p>
                        <p>Have not receive any codes..?</p>
                        <p align="center"><a href="#" onclick="resend_sms();" class="btn btn-info"><i class="fa fa-envelope-o"></i> Resend codes</a></p>
                    </div>
                </div>
            </div> 
        </div><!-- /.modal-content --> 
    </div><!-- /.modal-dialog --> 
</section>
<script>
    validate_this_number = function() {
        var ver_codes = $('#verification_codes').val();
        $('#ajax_modal_results').html(LOADER);
        if (ver_codes == '') {
            $('#ajax_modal_results').html('<br/><div class="alert alert-danger">\n\
                      <button type="button" class="close" data-dismiss="alert">×</button>\n\
                        <i class="fa fa-ban-circle"></i>Enter codes first</div>');
        }
        $.get(url, {pg: 'register', process: 'verification_code', ver_codes: ver_codes}, function(data) {
            if (data == 1) {
                window.location.reload();
            } else {
                //we get an error
                $('#ajax_modal_results').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><i class="fa fa-ban-circle"></i>  <a href="#" class="alert-link">Codes are not valid</a>.</div>');
            }
        });
    };
    function resend_sms() {
        $('#ajax_modal_results').html(LOADER);
        $.get(url, {pg: 'register', process: 'resend_codes', phone_number: <?= $phone_number ?>}, function(data) {
            $('#ajax_modal_results').html(data);
        });
    }
</script>