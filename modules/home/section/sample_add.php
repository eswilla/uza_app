<div class="modal-dialog"> 
    <div class="modal-content">
        <div class="modal-header">
            <?php 
            echo isset($_GET['sec']) ? 
                '<a href="'.HOME.'"><button type="button" class="close" data-dismiss="modal">&times;</button></a>':
                '<button type="button" class="close" data-dismiss="modal">&times;</button>';
            ?>

            <h4 class="modal-title">Sample add to show your customers</h4> 
        </div> 
        <div class="modal-body">
            <?php
            if (isset($ses_user)) {
                ?>
              
                    <section class="panel bg-dark m-b-n-lg no-border device animated fadeInUp"> 
                        <img src="media/doc/market/banner.jpg" style="opacity:1;" class="img-full" >
                    </section> 
                <?php
            } else {
                echo 'Signin to view your sample add for your customers to view';
            }
            ?>
        </div>
        <div class="modal-footer"> 
            <a href="<?= HOME ?>media/doc/market/banner.pdf" class="btn btn-default">Download</a>
            <?php
            echo isset($_GET['sec']) ?
                    '<a href="' . HOME . '" class="btn btn-default" data-dismiss="modal">Close</a>' :
                    '<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>';
            ?> 
        </div> 
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<?php
css_media('print', 'print');
?>