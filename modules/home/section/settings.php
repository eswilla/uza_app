<form data-validate="parsley"> 
    <section class="panel panel-default"> 
        <header class="panel-heading"> 
            <span class="h4">Comments</span>
        </header> 
        <div id="ajax_results2"></div>
        <div class="panel-body"> 
            <p class="text-muted">Please tell us what we need to add or modify to satisfy your need or improve this service</p> 
            <div class="form-group ">

                <label>Message</label> 
                <textarea class="form-control"  id="comments_area"rows="6" data-minwords="6" data-required="true" placeholder="Type your message"></textarea>  

            </div> 
        </div> 
        <footer class="panel-footer text-right bg-light lter"> 
            <button type="button" class="btn btn-success btn-s-xs sett" id="comments">Submit</button>
        </footer> 
    </section> 
</form>