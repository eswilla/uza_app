<?php
include_once 'modules/landing/banner/after_login.php';

?> 
<div style="margin-top: 10%;">
    <section class="panel panel-default">
        <div class="panel-body">
            <div class="clearfix text-center m-t"> 
                <div class="inline"> 
                    <div class="easypiechart easyPieChart" data-percent="75" data-line-width="5" data-bar-color="#4cc0c1" data-track-color="#f5f5f5" data-scale-color="false" data-size="130" data-line-cap="butt" data-animate="1000" style="width: 130px; height: 130px; line-height: 130px;">
                        <div class="thumb-lg">
                            <img src="media/images/avatar.jpg" class="img-circle">
                        </div> <canvas width="130" height="130">
                            
                        </canvas></div> <div class="h4 m-t m-b-xs"><?=$business->name?></div> 
                        <small class="text-muted m-b"><?=$business->slogan?></small>
                </div> 
            </div> 
        </div> 
        <?php $sms=  sms::find_where("business_id='".$id."'");?>
        <footer class="panel-footer bg-info text-center"> 
            <div class="row pull-out"> 
                <div class="col-xs-4"> 
                    <div class="padder-v"> 
                        <span class="m-b-xs h3 block text-white"><a href="<?=HOME?>help&sec=features"><i class="fa fa-share-square-o"></i></a></span>
                        <small class="text-muted">What's karibuSMS</small> 
                    </div>
                </div> 
                <div class="col-xs-4 dk">
                    <div class="padder-v"> 
                        <span class="m-b-xs h3 block text-white"><?= count($subscribers); ?></span> 
                        <small class="text-muted">Subscribers</small>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="padder-v">
                        <span class="m-b-xs h3 block text-white"><?=  count($sms)?></span> 
                        <small class="text-muted">Sent messages</small> 
                    </div> 
                </div> 
            </div> 
        </footer> 
    </section> 
    <section class="panel panel-info portlet-item"> 
        <header class="panel-heading"> Information </header> 
        <div class="list-group bg-white">
            <a href="#" class="list-group-item"> <i class="fa fa-fw fa-envelope"></i> Send SMS with keyword <span style="font-size: 17px; font-weight: bold;">KARIBU <?=$business->username?> to 15573</span> and be connected with us </a>
            <a href="#" class="list-group-item"> <i class="fa fa-fw fa-eye"></i> Be the first to be informed about us for Free with SMS for any new offer and promotions we have</a> 
        </div> 
    </section>
</div>
<div style="clear: both"></div>
<?php
include_once 'modules/landing/footer.php';
?> 