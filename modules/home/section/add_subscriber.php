<div class="modal-dialog" >
    <div class="modal-content">
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal">&times;</button> 
            <h4 class="modal-title">Add New Contact</h4>
        </div> 
        <div class="modal-body"> 
            <section class="panel panel-default">
                <header class="panel-heading bg-light"> 
                    <ul class="nav nav-tabs pull-right">
          <li class="active"><a href="#messages-1" data-toggle="tab"><i class="fa fa-mobile"></i> Add Numbers</a></li>
            <li class=""><a href="#excel" data-toggle="tab"><i class="fa fa-mobile"></i> Upload excel file</a></li>
                        <li class=""><a href="#profile-1" data-toggle="tab"><i class="fa fa-cloud-upload"></i> Using fliers</a></li> 
                    </ul>
                    <span class="hidden-sm">Choose Method</span>
                </header> 
                <div class="panel-body">
                    <div class="tab-content"> 
                        <div class="tab-pane active" id="messages-1">
                            <span id="ajax_add_number_result"></span>
                            <div class="panel-body" id="number_add_div"> 
                                <form class="form-inline" role="form"> 
                                    <div class="form-group col-md-4"> 

                                        <input type="text" id="name" class="form-control" id="exampleInputEmail2" placeholder="Name(option)"> 
                                    </div> 
                                    <div class="form-group"> 

                                        <input type="text" id="number" class="form-control" id="exampleInputPassword2" placeholder="Phone number"> 
                                        <small id="number_tip"></small></div>
                                    <button type="button" id="add_number_button" class="btn  btn-success">Add</button>
                                    <small style="color: #00a6ce; cursor: pointer;" id="multiple" title="click to add multiple numbers">multiple</small>
                                </form> 
                            </div>
                        </div> 
                        <div class="tab-pane" id="excel">
                            <?php 
                           
                            if(karibu::$allow_file_upload==1){ ?>
                            <p>Only XLS files are required</p>
                            <p>Column with phone numbers  and name should be named 
                                <span class="label label-info">phonenumber</span> and  <span class="label label-info">name</span></p>
                            <p id="target_result"></p>
                            <form class="form-inline" id="upload_excel_file" method="post" action="<?= $AJAX ?>pg=profile&process=upload&busid=<?= $ses_user->id ?>" role="form"> 
                                <div class="form-group"> 
                                    <label class="sr-only" for="excel_file">Excel file with contacts</label>
                                    <input type="file" name="file" class="form-control" placeholder="upload profile">
                                </div>
                                <button type="submit" class="btn btn-default" id="upload_excel">Upload</button> 
                            </form>
                            <?php }else { ?>
                            <div class="alert alert-warning"><p>Your plan do not allow you to upload numbers from excel file.  <a href="#layout" class="label label-info" style="color: white; font-size:17px" onclick="get_send({pg: 'payment', section: 'pay'}, 'content')" data-dismiss="modal">Upgrade now</a></p></div> 
                            <?php  }
?>
                        </div> 
                        <div class="tab-pane" id="profile-1">
                            <p>
                                <br/>
                                This is the simple option for you to get your customers easily
                                <br/>
                            <ul>
                                <li><a href="<?= HOME ?>home&sec=sample_add" style="color: #00a6ce; cursor: pointer;">Click here </a>to download or print your flier</li>
                                <li>Put this flier in your business where your customers can easily see it</li>
                                <li>Your customers will subscribe themselves to your business with their mobile phone</li>
                            </ul>

                            </p>

                        </div> 

                    </div>
                </div> 
                <div id="ajax_payment_status"></div>
            </section>

        </div>
        <div class="modal-footer"> 
            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a> 
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<?php js_media('jquery.form'); ?>
<script>
    add_number = function() {
        $('#add_number_button').click(function() {
           
            var name = $('#name').val();
            var number = $('#number').val();
            $('#number_add_div').hide();
            $('#ajax_add_number_result').html(LOADER);
            $.get(url, {pg: 'home', process: 'add_number', name: name, number: number}, function(data) {
                $('#ajax_add_number_result').html(data).fadeOut(3000);
                $('#name,#number').val('');
                $('#number_add_div').show();
                $('#add_number_button').html('Add more');
            });
        });
    };
    $.fn.extend({
        toggleText: function(a, b) {
            if (this.html() == a) {
                this.html(b)
            }
            else {
                this.html(a)
            }
        }
    });
    multiple = function() {
        $('#multiple').click(function() {
            $('#name').toggle();
            $('#number_tip').toggleText('Add multiple number separated by comma', '');
            $('.form-group').toggleClass('col-lg-8');
            $('#multiple').toggleText('one', 'multiple');
            $(this).attr('title', $(this).attr('title') == 'click to add multiple numbers' ? 'click to add one number' : 'click to add multiple numbers');
        });
    };
    upload_excel_file=function(){
       $('#upload_excel').click(function() {
           $('#target_result').html(LOADER);
            $('#upload_excel_file').ajaxForm({
                target: '#target_result'
            });
        });  
    };
    $(document).ready(upload_excel_file);
    $(document).ready(add_number);
    $(document).ready(multiple);
</script>