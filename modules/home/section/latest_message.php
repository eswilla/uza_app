<?php

?>

<section class="panel panel-default">
    <header class="panel-heading"> <span class="label bg-dark"></span> Latest messages</header> 
    <div class="slimScrollDiv" style="position: relative; overflow: hidden;
         width: auto; height: 230px;">
        <section class="panel-body slim-scroll" data-height="230px" 
                 style="overflow: scroll; width: auto; height: 230px;">
            <div id="ajax_server_sms_request"></div>
            <?php 
            $sms=  sms::find_where("business_id='".$ses_user->id."' ORDER BY sms_time DESC");
            if(!empty($sms)){
            foreach ($sms as $msg) {
             
            ?>
            <article class="media"> 
                <span class="pull-left thumb-sm">
                    <img src="media/images/avatar_default.jpg" class="img-circle"></span>
                <div class="media-body">
                    <div class="pull-right media-xs text-center text-muted">
                        <strong class="h4"><?=$input->make_time_ago($msg->time)?></strong><br> 
                        <small class="label bg-light"></small> 
                    </div> 
                    <a href="#" class="h4"><?=  word_limiter($msg->content)?></a> 
                    <small class="block"><a href="#" class=""><?=$ses_user->name?></a> 
                        <span class="label label-success">Admin</span>
                    </small> 
                    <small class="block m-t-sm">
                       <?=$msg->content?>
                    </small> 
                </div> 
            </article>
            <div class="line pull-in"></div> 
            <?php }}else{
    echo '<p id="no_post">You have posted no message yet</p>';
            } ?>
        </section>
        <div class="slimScrollBar" style="background-color: rgb(0, 0, 0);  width: 7px; position: absolute; top: 35px; opacity: 0.4; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; height: 170.6451612903226px; background-position: initial initial; background-repeat: initial initial;"></div>
        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; background-color: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px; background-position: initial initial; background-repeat: initial initial;"></div>

    </div> 
</section>