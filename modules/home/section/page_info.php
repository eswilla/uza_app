<div class="modal-dialog" >
    <div class="modal-content">
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal">&times;</button> 
            <h4 class="modal-title">Your page information</h4>
        </div> 
        <div class="modal-body"> 
            <ul>
                <?php
                if (isset($ses_user)) {
                    ?>
                    <p>Business name: <strong><?=$ses_user->name?></strong></p>
                    <p>Business username: <strong><?=$ses_user->username?></strong></p>
                    <p>Business slogan: <strong><?=$ses_user->slogan?></strong></p>
                    <p><br/></p>
                    <p>Subscription keyword: <strong> KARIBU <?=  strtoupper($ses_user->name)?></strong></p>
                <?php
                } else {
                    echo 'Signin to view your page information';
                }
                ?>
            </ul>
        </div>
        <div class="modal-footer"> 
            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a> 
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->