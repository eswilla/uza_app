<?php
$subscriptions=  subscription::find_where(array('business_id'=>$ses_user->id));
?>
<div class="modal-dialog">
    <div class="modal-content"> 
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">List of your contacts</h4> 
        </div> 
<section class="panel panel-default"> 
    <header class="panel-heading"> Responsive Table </header>
<!--    <div class="row text-sm wrapper">
        <div class="col-sm-4 m-b-xs">
            <div class="btn-group" data-toggle="buttons"> 
                <label class="btn btn-sm btn-default active"><input type="radio" name="options" id="option1">Uploaded</label>
                <label class="btn btn-sm btn-default"> <input type="radio" name="options" id="option2"> Subscribe </label>
            </div>
        </div> 
        <div class="col-sm-3"> 
            <div class="input-group"> 
                <input type="text" class="input-sm form-control" placeholder="Search">
                <span class="input-group-btn"> <button class="btn btn-sm btn-default" type="button">Go!</button> </span> 
            </div>
        </div> 
    </div>-->
    
    <div class="table-responsive">
        <table class="table table-striped b-t b-light text-sm">
            <thead>
                <tr> 
                    <th width="20"><input type="checkbox"></th> 
                    <th class="th-sortable active" data-toggle="class">Name <span class="th-sort"> <i class="fa fa-sort-down text"></i> <i class="fa fa-sort-up text-active"></i> <i class="fa fa-sort"></i> </span> </th>
                    <th>Phone number</th> 
                    <th>Date</th>
                    <th></th>
                    <th width="30"></th> </tr> 
            </thead> 
            <tbody>
                <?php 
             
                foreach ($subscriptions as $subscription) {
                    $subscriber_info=  subscriber::find_by_id($subscription->subscriber_id);
                    $subscriber=  array_shift($subscriber_info);
                ?>
                <tr>
                    <td><input type="checkbox" name="post[]" value="<?=$subscriber->id?>"></td> 
                    <td><?=$subscriber->othernames?></td>
                    <td><?=$subscriber->phone_number?></td> 
                    <td><?=$subscriber->regtime?></td>
                    <td><?=$subscriber->is_uploaded==1 ? '<i class="fa fa-upload" title="Uploaded"></i>':'';?></td>
                    <td> <a href="#" class="active" data-toggle="class"><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a> </td> 
                </tr> 
                <?php } ?>
            </tbody> 
        </table>
    </div>
    <footer class="panel-footer"> 
<!--        <div class="row"> 
            <div class="col-sm-4 hidden-xs">
                <select class="input-sm form-control input-s-sm inline">
                    <option value="0">Bulk action</option> 
                    <option value="1">Delete selected</option>
                    <option value="2">Bulk edit</option> 
                    <option value="3">Export</option> 
                </select> 
                <button class="btn btn-sm btn-default">Apply</button>
            </div> 
            <div class="col-sm-4 text-center">
                <small class="text-muted inline m-t-sm m-b-sm">showing 20-30 of 50 items</small> 
            </div> 
            <div class="col-sm-4 text-right text-center-xs">
                <ul class="pagination pagination-sm m-t-none m-b-none"> 
                    <li><a href="#"><i class="fa fa-chevron-left"></i></a></li> 
                    <li><a href="#">1</a></li> 
                    <li><a href="#">2</a></li> 
                    <li><a href="#">3</a></li>
                    <li><a href="#"><i class="fa fa-chevron-right"></i></a></li> 
                </ul> </div> </div> -->
    </footer> 
</section>
        
        <div class="modal-footer"> 
            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a> 
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->