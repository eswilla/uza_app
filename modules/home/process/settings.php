<?php

/**
 *   karibu.com
 * * @author Ephraim Swilla <swillae1@gmail.com>
 */
if (!empty($_GET)) {

    $data = array();
    parse_str($_GET['datastring'], $data);
    $number = validate_phone_number($data['phone_number']);

    if (!is_array($number)) {
        //error, number is not valid
        echo json_encode(
                array(
                    'error' => '<div class="alert alert-danger"> 
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-ban-circle"></i> 
            <a href="#" class="alert-link">Invalid phone number</a>, enter it correctly.
            </div>',
                    'success' => ''
        ));
    } else {

        $new_data = array(
            'name' => $data['business_name'],
            'username' => preg_replace('/ /', '', substr($data['username'], 0, 10)),
            'phone_number' => $number[1],
            'country' => $number[0],
            'slogan' => $data['slogan']
        );
        $db->update('business', $new_data, "id='" . $ses_user->id . "'");
        echo json_encode(
                array(
                    'error' => '',
                    'success' =>'<div class="alert alert-success"><i class="fa fa-check"></i> <button type="button" class="close" data-dismiss="alert">×</button>
                          <i class="fa fa-ok-sign"></i><strong>Information successful updated</strong> </div>'
        ));
    }
}
?>
