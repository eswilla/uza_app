<?php

class profile_sms {

    private $content;
    public $result;
    private $subscribers = array();
    private $sms_id;

    public function __construct() {
        $this->result['message'] = '';
        $this->result['succss'] = '';
        $this->result['error'] = '';
        $this->content = $_GET['content'];
        if (isset($_GET['sub_id']) && !empty($_GET['sub_id'])) {
            $this->send_to_id($_GET['sub_id']);
        } else if (!empty ($ses_user) && $ses_user->messaging_type == 1) {
            $this->send_prosms();
        } else {
            $this->send_karibu_sms();
        }
    }

    public function save_sms() {
        global $ses_user;
        global $db;
        $data = array(
        'content' => $this->content,
        'business_id' => $ses_user->id
        );
        $result = $db->insert('sms', $data);
        $this->sms_id = $db->id();
        return $result;
    }

    public function get_subscribers() {
        global $ses_user;
        $subscriber_data = array(
        "business_id" => $ses_user->id,
        "accept_sms" => 1
        );
        $subscriptions = subscription::find_where($subscriber_data);
        foreach ($subscriptions as $subscription) {
            $subscriber_info = subscriber::find_by_id($subscription->subscriber_id);
            $subscriber = array_shift($subscriber_info);
            $this->subscribers[] = $subscriber->phone_number;
        }
        return $this->subscribers;
    }

    private function send_to_id($id) {
        global $ses_user;
        $subscriber_info = subscriber::find_by_id($id);
        $subscriber = array_shift($subscriber_info);

        $return["posts"] = array();
        $post = array();
        $post["phonenumber"] = $subscriber->phone_number;
        array_push($return["posts"], $post);

        $message = array("Notice" => $this->format_smart_sms(), "server" => $return);
        $push = gcm::push_to_id($message, $ses_user->gcm_id);
        if ($push) {
            $this->result['message'] = '<div class="alert alert-success">Message sent</div>';
            echo json_encode($this->result);
        } else {
            $this->result['error'] = '<div class="alert alert-danger"> 
Message failed to be sent .
            </div>';
            echo json_encode($this->result);
        }
    }

    private function get_message($sms) {
        $this->get_subscribers();
        if ($this->save_sms() && count($this->subscribers) > 0) {
            $return["posts"] = array();

            foreach ($this->subscribers as $subscriber) {
                $post = array();
                $post["phonenumber"] = $subscriber;
                array_push($return["posts"], $post);
            }
            $message = array("Notice" => $sms, "server" => $return);
            return $message;
        }
    }

    public function check_errors() {
        $subscribers = $this->get_subscribers();
        if (empty($this->subscribers)) {

            $this->result['error'] = '<div class="alert alert-danger"> 
You have no subscribers add phone numbers of your customer .
            </div>';
            echo json_encode($this->result);
            exit;
        }
        global $ses_user;
        $check_sms = bsextrsms::find_where(array('business_id' => $ses_user->id));
        if (!empty($check_sms)) {
            $sms = array_shift($check_sms);
            if (count($this->subscribers) > $sms->total_sms) {
                $this->result['error'] = '<div class="alert alert-danger"> 
You have less SMS to send that number of subscribers you have</a>, Add more SMS first
            </div>';
                echo json_encode($this->result);
                exit;
            }
        }
    }

    public function check_payment_status() {
        global $ses_user;
        $bundle_info = bp::find_where(array("business_id" => $ses_user->id, "expired" => 0));
        if (!empty($bundle_info)) {

            $bundle = array_shift($bundle_info);
            $total_sms = $bundle->sms_per_day + $ses_user->total_sms;
        } else {
            echo json_encode(array(
            'message' => '',
            'success' => '',
            'error' => '<div class="alert alert-danger"> 
                      You have no SMS to send Please buy SMS first.
            </div>'
            ));
            exit;
            $total_sms = $ses_user->total_sms;
        }
    }

    public function format_smart_sms() {
        global $ses_user;
        $short_url = 'www.karibusms.com/' . $ses_user->username;
        $message = $ses_user->username . ': 
' . $this->content . '
' . $short_url;
        return $message;
    }

    public function send_karibu_sms() {
        global $ses_user;
        $this->save_sms();
        $subscriber_data = array(
        "business_id" => $ses_user->id,
        "accept_sms" => 1
        );
        $subscriptions = subscription::find_where($subscriber_data);

        //$this->get_subscribers();

        $return["posts"] = array();
        $i = 1;
        $sent_ids = array();
        $all_ids = array();
        foreach ($subscriptions as $subscription) {
            $post = array();

            $all_ids = array_merge($all_ids, array($subscription->subscriber_id));

            if ($i < 50) {
                $sent_ids = array_merge($sent_ids, array($subscription->subscriber_id));
                $users_info = subscriber::find_by_id($subscription->subscriber_id);

                $user = array_shift($users_info);
                $post["phonenumber"] = '+' . $user->phone_number;
                $post["name"] = '+' . $user->othernames;
                array_push($return["posts"], $post);
            }
            $i++;
        }

        //check if contacts exceed 100 in $all_ids
        $remain_ids = array_diff($all_ids, $sent_ids);
        if (count($remain_ids) > 1) {
            global $db;
            $data = array(
            'sms_id' => $this->sms_id,
            'subscriber_id' => implode(',', $remain_ids),
            'issms_to_send' => 1,
            'type' => 'karibusms',
            'business_id' => $ses_user->id
            );
            $db->insert('qsms', $data);
        }

        $message = array("Notice" => $this->format_smart_sms(), "server" => $return);
        $push = gcm::push_to_id($message, $ses_user->gcm_id);

        if ($push) {
            $this->result['message'] = '<div class="alert alert-success">Message sent</div>';
            echo json_encode($this->result);
        } else {
            $this->result['error'] = '<div class="alert alert-danger">Message failed to be sent.</div>';
            echo json_encode($this->result);
        }
    }

    private function check_gcm_status() {
        
    }

    public function send_prosms() {
        global $ses_user;
        global $db;
        $this->get_subscribers();
        foreach ($this->subscribers as $subscriber) {
            $user_info = subscriber::find_where(array('phone_number' => $subscriber));
            $user = array_shift($user_info);

            $sender = new sms_sender();
            $sender->set_from_name($ses_user->username);

            //replace @name with a customer name $user->name
            $sender->set_message($this->content);
            $sender->set_phone_number($subscriber);
            $send = $sender->send();
            if ($send == true) {
                $db->delete('qsms', "to_id='" . $user->id . "'");
            } else {
                error_record('Error occur in sending sms to user ' . $user->id . ' from page SMS id  ' . $this->sms_id, $send, TRUE);
            }
        }
    }

    private function update_sms_table() {
        $user_id = '';
        global $ses_user;

        foreach ($this->subscribers as $subscriber) {
            $data = array(
            'business_id' => $ses_user->id,
            'business_name' => $ses_user->username,
            'sms_id' => $this->sms_id,
            'content' => $this->content,
            'to_id' => $subscriber->user_id
            );
            $db->insert('qsms', $data);
            $user_id.=$subscriber->user_id . ",";
        }
        $user_id = rtrim($user_id, ',');
        $update = $db->update('sms', array('user_id' => $user_id), "id='" . $this->sms_id . "'");
    }

    public function que_sms() {
        
    }

    public function format_sms() {
        
    }

}

new profile_sms();
?>
