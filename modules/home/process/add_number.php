<?php

//we upload contacts names here

function add_new_number($phone_number, $name='') {
    global $db;
    global $ses_user;
    $validate = validate_phone_number(str_replace(' ', '', $phone_number));
    if (is_array($validate)) {

        //lets check if this number is in our database
        $subscriber_info = subscriber::find_where(array("phone_number"=>$validate[1]));
        if (!empty($subscriber_info)) {
            //this number is on our database
            $subscriber = array_shift($subscriber_info);
            $id = $subscriber->id;
        } else {
            //this is the new user
            $data = array(
                'othernames' => $name,
                'phone_number' => $validate[1],
                'country' => $validate[0]
            );
            $db->insert('subscriber', $data);
            $id = $db->id();
        }
        $subscriber_data = array(
            'business_id' => $ses_user->id,
            'subscriber_id' => $id
        );
        $db->insert('subscription', $subscriber_data);
        echo '<div class="alert alert-success">' . $validate[1] . ' inserted successful</div>';
    } else {
        echo '<div class="alert alert-danger"> 
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-ban-circle"></i> 
            <a href="#" class="alert-link">Error, This number ' . $validate[1] . ' is invalid.</a>
            </div>';
    }
}

if (!empty($_GET)) {
    $name = isset($_GET['name']) ? $_GET['name'] : '';
    $numbers = explode(',', $_GET['number']);

    foreach ($numbers as $phone_number) {
        add_new_number($phone_number, $name);
    }
}
