<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- .aside --> 
<aside class="bg-light lter b-r aside-md hidden-print" id="nav">
    <section class="vbox"> 
        <header class="header bg-primary lter text-center clearfix">
            <div class="btn-group"></div>
        </header>
        <section class="w-f scrollable">
            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" 
                 data-distance="0" data-size="5px" data-color="#333333" style="overflow: hidden; width: auto; height: 277px;">
                <!-- nav --> <nav class="nav-primary hidden-xs"> 
                    <ul class="nav"> 
                        <?php if (isset($_SESSION['pesasms']) && $_SESSION['pesasms'] == TRUE) { ?>
                            <li class="active"> 
                                <a href="<?= HOME ?>" class="active"> 

                                    <i class="fa fa-home"><b class="bg-success"></b></i>                                
                                    <span>Home</span> 
                                </a>
                            </li>
                            <li class=""> 
                                <a href="#layout" onclick="get_send({pg: 'pesasms', part: 'admin'}, 'content');"> 
                                    <i class="fa fa-adn"> <b class="bg-info"></b> </i> 
                                    <span class="pull-right"></span> 
                                    <span>PesaSMS admin</span> 
                                </a>
                            </li>
<!--                            <li class=""> 
                                <a href="#layout" onclick="get_send({pg: 'pesasms', part: 'message'}, 'content');"> 
                                    <i class="fa fa-envelope"> <b class="bg-primary"></b> </i> 
                                    <span class="pull-right"></span> 
                                    <span>Messages</span> 
                                </a>
                            </li>-->
                        <?php } else { ?>
                            <li class="active"> 
                                <a href="<?= HOME ?>" class="active"> 

                                    <i class="fa fa-home"><b class="bg-success"></b></i>                                
                                    <span>Home</span> 
                                </a>
                            </li>
                            <li class=""> 
                                <a href="#layout" onclick="get_send({pg: 'profile', section: 'contacts'}, 'content');"> 
                                    <i class="fa fa-phone"> <b class="bg-info"></b> </i> 
                                    <span class="pull-right"></span> 
                                    <span>Contacts</span> 
                                </a>
                            </li>
                            <li class=""> 
                                <a href="#layout" onclick="get_send({pg: 'payment', section: 'pay'}, 'content');"> 
                                    <i class="fa fa-dollar"> <b class="bg-primary"></b> </i> 
                                    <span class="pull-right"></span> 
                                    <span>Payments</span> 
                                </a>
                            </li>
                            <li class=""> 
                                <a href="#layout" onclick="get_send({pg: 'profile', section: 'master_settings'}, 'content');"> 
                                    <i class="fa fa-asterisk icon"> <b class="bg-warning"></b> </i> 
                                    <span class="pull-right"></span> 
                                    <span>Settings</span> 
                                </a>
                            </li>
                            <?php if ($ses_user->id == 1) {
                                ?>
                                <li class=""> 
                                    <a href="#layout" onclick="get_send({pg: 'admin', file: 'admin'}, 'content');"> 
                                        <i class="fa fa-dashboard icon"> <b class="bg-info"></b> </i> 
                                        <span class="pull-right"></span> 
                                        <span>Admin panel</span> 
                                    </a>
                                </li>
                                <?php
                            }
                        }
                        ?>
                        <li> 
                            <a href="<?= HOME ?>logout" >
                                <i class="fa fa-power-off"><b class="bg-danger"></b></i> 
                                <span>Sign out</span> 
                            </a> 
                        </li>      
                    </ul>
                </nav> <!-- / nav --> 
            </div> 
        </section> 
        <footer class="footer lt hidden-xs b-t b-dark"> 
            <!--            <div id="chat" class="dropup"> 
                            <section class="dropdown-menu on aside-md m-l-n"> <section class="panel bg-white"> <header class="panel-heading b-b b-light">Active chats</header> <div class="panel-body animated fadeInRight"> <p class="text-sm">No active chats.</p> <p><a href="#" class="btn btn-sm btn-default">Start a chat</a></p> </div> </section> 
                            </section> </div>
                        <div id="invite" class="dropup"> <section class="dropdown-menu on aside-md m-l-n"> <section class="panel bg-white"> <header class="panel-heading b-b b-light"> John <i class="fa fa-circle text-success"></i> </header> <div class="panel-body animated fadeInRight"> <p class="text-sm">No contacts in your lists.</p> <p><a href="#" class="btn btn-sm btn-facebook"><i class="fa fa-fw fa-facebook"></i> Invite from Facebook</a></p> </div> </section> </section> 
                        </div>-->
            <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon"> <i class="fa fa-angle-left text"></i> <i class="fa fa-angle-right text-active"></i> </a> 
            <!--            <div class="btn-group hidden-nav-xs"> 
                            <button type="button" title="Chats" class="btn btn-icon btn-sm btn-dark" data-toggle="dropdown" data-target="#chat"><i class="fa fa-comment-o"></i></button> 
                            <button type="button" title="Contacts" class="btn btn-icon btn-sm btn-dark" data-toggle="dropdown" data-target="#invite"><i class="fa fa-facebook"></i></button>
                        </div> -->
        </footer>

    </section> 
</aside> <!-- /.aside --> 
