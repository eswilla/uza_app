<?php

/**
 *   karibu.com
 * * @author Ephraim Swilla <swillae1@gmail.com>
 */
css_media('bootstrap_calendar');
?>
<section class="vbox">
    <?php

    include_once 'modules/landing/banner/after_login.php';
    ?> 
    <section> 
        <section class="hbox stretch">
            <?php

            include_once 'modules/landing/aside.php';
            ?>
            <section id="content"  style="background: #f7f7f7;">
                <section class="vbox"> 
                    <section class="scrollable padder"> 
                        <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                            <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Workset</li> 
                        </ul> 
                        <div class="m-b-md"> 
                            <h3 class="m-b-none">Workset</h3> <small>Welcome back, Noteman</small>
                        </div>

                        <div class="row"> 
                            <div class="col-md-8"> 
                                <section class="panel panel-default"> 
                                    <header class="panel-heading font-bold">Statistics</header> 
                                    <div class="panel-body"> 
                                        <section class="panel panel-default"> 
                                            <form> 
                                                <textarea class="form-control no-border" rows="3" placeholder="What are you doing..."></textarea> 
                                            </form> 
                                            <footer class="panel-footer bg-light"> 
                                                <button class="btn btn-info pull-right btn-sm">POST</button>
                                                <ul class="nav nav-pills nav-sm">
                                                    <li><a href="#"><i class="fa fa-camera text-muted"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-video-camera text-muted"></i></a></li> 
                                                </ul>
                                            </footer>
                                        </section>
                                    </div> 
                                    <footer class="panel-footer bg-white no-padder"> 
                                        <div class="row text-center no-gutter"> 
                                            <div class="col-xs-3 b-r b-light"> 
                                                <span class="h4 font-bold m-t block">5,860</span>
                                                <small class="text-muted m-b block">Orders</small>
                                            </div> 
                                            <div class="col-xs-3 b-r b-light"> 
                                                <span class="h4 font-bold m-t block">10,450</span> 
                                                <small class="text-muted m-b block">Sellings</small>
                                            </div> <div class="col-xs-3 b-r b-light">
                                                <span class="h4 font-bold m-t block">21,230</span>
                                                <small class="text-muted m-b block">Items</small>
                                            </div> 
                                            <div class="col-xs-3">
                                                <span class="h4 font-bold m-t block">7,230</span>
                                                <small class="text-muted m-b block">Customers</small> 
                                            </div> 
                                        </div>
                                    </footer> 
                                </section> 
                            </div> 

                            <div class="col-md-4"> 
                             <section class="panel panel-default">
                                 <header class="panel-heading"> <span class="label bg-dark">15</span> Latest post</header> 
                                 <div class="slimScrollDiv" style="position: relative; overflow: hidden;
                                      width: auto; height: 230px;">
                                     <section class="panel-body slim-scroll" data-height="230px" 
                                              style="overflow: hidden; width: auto; height: 230px;">
                                         <article class="media"> 
                                             <span class="pull-left thumb-sm">
                                                 <img src="media/images/avatar_default.jpg" class="img-circle"></span>
                                             <div class="media-body">
                                                 <div class="pull-right media-xs text-center text-muted">
                                                     <strong class="h4">12:18</strong><br> 
                                                     <small class="label bg-light">pm</small> 
                                                 </div> 
                                                 <a href="#" class="h4">Bootstrap 3 released</a> 
                                                 <small class="block"><a href="#" class="">John Smith</a> 
                                                     <span class="label label-success">Circle</span>
                                                 </small> 
                                                 <small class="block m-t-sm">
                                                     Sleek, intuitive, and powerful mobile-first front-end framework for faster and easier web development.</small> 
                                             </div> 
                                         </article>
                                         <div class="line pull-in"></div> 
                                         <article class="media">
                                             <span class="pull-left thumb-sm">
                                                 <i class="fa fa-file-o fa-3x icon-muted"></i></span>
                                                         <div class="media-body"> 
                                                             <div class="pull-right media-xs text-center text-muted">
                                                                 <strong class="h4">17</strong><br> <small class="label bg-light">feb</small> </div> <a href="#" class="h4">Bootstrap documents</a> 
                                                             <small class="block"><a href="#" class="">John Smith</a> 
                                                                 <span class="label label-info">Friends</span></small> 
                                                             <small class="block m-t-sm">There are a few easy ways to quickly get started with Bootstrap, each one appealing to a different skill level and use case. Read through to see what suits your particular needs.</small> </div> </article> <div class="line pull-in"></div> <article class="media"> <div class="media-body"> <div class="pull-right media-xs text-center text-muted">
                                                                         <strong class="h4">09</strong><br> <small class="label bg-light">jan</small> </div> <a href="#" class="h4 text-success">Mobile first html/css framework</a> <small class="block m-t-sm">Bootstrap, Ratchet</small> </div>
                                                             </article>
                                     </section>
                                     <div class="slimScrollBar" style="background-color: rgb(0, 0, 0); width: 7px; position: absolute; top: 35px; opacity: 0.4; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; height: 170.6451612903226px; background-position: initial initial; background-repeat: initial initial;"></div>
                                     <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; background-color: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px; background-position: initial initial; background-repeat: initial initial;"></div>
                                                             
                                 </div> 
                             </section>
                            </div> 

                        </div> 

                        </div> 
                    </section> 
                </section> 
                <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> 
            </section> 
            <aside class="bg-light lter b-l aside-md hide" id="notes"> 
                <div class="wrapper">Notification</div> 
            </aside> 
        </section>
    </section>
</section>
<script src="note/js/app.v2.js"></script>
<?php

$js_array = array(
    'charts/easypiechart/jquery.easy-pie-chart',
    'charts/sparkline/jquery.sparkline.min',
    'charts/flot/jquery.flot.min',
    'charts/flot/jquery.flot.tooltip.min',
    'charts/flot/jquery.flot.resize',
    //'charts/flot/jquery.flot.grow',
   // 'charts/flot/demo',
   // 'calendar/bootstrap_calendar',
   // 'calendar/demo',
   // 'sortable/jquery.sortable'
    );
foreach ($js_array as $js) {
    js_media($js);
}
?>