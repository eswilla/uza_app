<footer id="footer"> 
        <div class="bg-primary text-center">
            <div class="container wrapper"> 
                <div class="m-t-xl m-b"> Karibu, a way to be connected with your customers. 
               <a href="<?=HOME?>api" target="_blank" class=" b-white bg-empty m-sm">Get Our Developer API</a>
                      <!--               <a href="index.html" target="_blank" class="btn btn-lg btn-warning b-white bg-empty m-sm">Live Preview</a> -->
                </div> 
            </div> 
            <i class="fa fa-caret-down fa-4x text-primary m-b-n-lg block"></i> 
        </div> 
        <div class="bg-dark dker wrapper"> 
            <div class="container text-center m-t-lg">
                <div class="row m-t-xl m-b-xl"> 
                    <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
                        <i class="fa fa-map-marker fa-3x icon-muted"></i> 
                        <h5 class="text-uc m-b m-t-lg">Find Us</h5> 
                        <p class="text-sm">First Floor, Science building(COSTECH) <br> Mobile Contact — +255 759 55 33 55 or +255 658 55 33 55 </p> 
                    </div> 
                    <div class="col-sm-4" data-ride="animated" data-animation="fadeInUp" data-delay="600"> 
                        <i class="fa fa-envelope-o fa-3x icon-muted"></i> 
                        <h5 class="text-uc m-b m-t-lg">Mail Us</h5>
                        <p class="text-sm"><a href="mailto:info@karibu.com">info@karibusms.com</a></p> 
                    </div> 
                    <div class="col-sm-4" data-ride="animated" data-animation="fadeInRight" data-delay="900">
                        <i class="fa fa-globe fa-3x icon-muted"></i> 
                        <h5 class="text-uc m-b m-t-lg">Join Us</h5> 
                        <p class="text-sm">Send your interest <br>
                            Invest: <a href="mailto:investor@karibu.com">investor@karibusms.com</a></p> 
                        <p class="text-sm">Jobs: <a href="mailto:investor@karibu.com">jobs@karibusms.com</a></p> 
                   
                    </div> 
                </div> 
                <div class="m-t-xl m-b-xl">
                    <p> <a target="_blank" href="https://www.facebook.com/karibuSMS" class="btn btn-icon btn-rounded btn-facebook bg-empty m-sm">
                            <i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://twitter.com/KaribuSMS" class="btn btn-icon btn-rounded btn-twitter bg-empty m-sm"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="https://plus.google.com/109761774317687567059" rel="publisher" class="btn btn-icon btn-rounded btn-gplus bg-empty m-sm"><i class="fa fa-google-plus"></i></a>
                    </p> 
                    <p> 
                        <a href="#content" data-jump="true" class="btn btn-icon btn-rounded btn-dark b-dark bg-empty m-sm text-muted">
                            <i class="fa fa-angle-up"></i></a> 
                    </p> 
                </div> 
            </div> 
        </div> 
    </footer> <!-- / footer --> 