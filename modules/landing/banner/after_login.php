<?php
$id = !isset($ses_user) ? $business->id : $ses_user->id;
$name = !isset($ses_user) ? $business->name : $ses_user->name;
if (isset($ses_user)) {
    $business = $ses_user;
}
$subscribers = subscription::find_where("business_id='" . $business->id . "'");
$link = $business->profile_pic != '' ? 'media/images/business/' . $business->id . '/' . $business->profile_pic : 'media/images/avatar_default.jpg';

?>
<header class="bg-dark dk header navbar navbar-fixed-top-xs"> 
    <div class="navbar-header aside-md"> 
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
            <i class="fa fa-bars"></i> </a>
        <a href="<?=HOME?>" class="navbar-brand">
            <img src="media/images/logo.png" class="m-r-sm">KaribuSMS</a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user"> 
            <i class="fa fa-cog"></i> </a> 
    </div> 
    <?php if(isset($ses_user)){ ?>
    <ul class="nav navbar-nav hidden-xs">
        <li> 
            <div class="m-t m-l">
                <a href="<?= HOME ?>payment" class="dropdown-toggle btn btn-xs btn-primary" title="Features and payment plan">
                    <i class="fa fa-long-arrow-up"></i></a>
            </div> 
        </li> 
    </ul> 
    <?php } ?>
    <ul class="nav navbar-nav navbar-right hidden-xs nav-user">
        <?php if (isset($ses_user)) { ?>
            <li class="hidden-xs">

                <a href="#" class="dropdown-toggle dk" data-toggle="dropdown" id="not_no"> 
                    <i class="fa fa-bell"></i>
                    <span class="badge badge-sm up bg-danger  count" ></span>
                </a> 
                <section class="dropdown-menu aside-xl"> 
                    <section class="panel bg-white">
                        <header class="panel-heading b-light bg-light">
                            <strong>You have <span class="count"></span> notifications</strong> 
                        </header> 
                        <div id="notification_ajax_results"></div>
                        <footer class="panel-footer text-sm">
                            <a href="#" class="pull-right"><i class="fa fa-cog"></i></a>
                            <a href="#notes" onclick="get_send({pg:'landing',process:'all_ntfn'},'ajax_load_all_notifications');" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                        </footer> 
                    </section> 
                </section> 
            </li> 
        <?php } ?>
                  <li class="dropdown hidden-xs"> 
                        <a href="#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-search"></i></a> 
                        <section class="dropdown-menu aside-xl animated fadeInUp"> 
                            <section class="panel bg-white"> 
                                <form role="search">
                                    <div class="form-group wrapper m-b-none"> 
                                        <div class="input-group">
                                            <input type="text" id="searchbox" class="form-control" placeholder="Search"> 
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button> 
                                            </span> 
                                        </div> 
                                    </div> 
                                </form>
                                <div id="sechdiv"></div>
                            </section> 
                        </section>
                    </li> 
        <li class="dropdown"> 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                <span class="thumb-sm avatar pull-left"> <img src="<?=$link?>">
                </span> 
                <?= $business->name ?>
                <b class="caret"></b> 
            </a> 
            <?php if(isset($ses_user)){ ?>
            <ul class="dropdown-menu animated fadeInRight"> 
                <span class="arrow top"></span> 
                <li> <a href="<?= HOME ?>">profile</a> </li>
                <li> <a href="<?= HOME ?>help">Help</a> </li>
                <li class="divider"></li>
                <li> <a href="<?= HOME ?>logout">Sign out</a> </li> 
            </ul>
            <?php } ?>
        </li> 
    </ul> 
</header>
<?php if (isset($ses_user)) { ?>
    <script>
        notification = function() {
            $.get(url, {pg: 'landing', process: 'count_notification'}, function(data) {
                $('.count').html(data);
            });
            $('#not_no').click(function() {
                $('#notification_ajax_results').html(LOADER);
                $.getJSON(url, {pg: 'landing', process: 'display_notification'}, function(data) {
                    $('#notification_ajax_results').html(data.message);
                    $('.count').html(0);
                }).error(function(data) {
                    console.log(data);
                });
            });
        };
        jQuery(document).ready(notification);
    </script>
<?php } ?>