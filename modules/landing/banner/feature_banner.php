<?php css_media('landing'); ?>
<header id="header" class="navbar navbar-fixed-top bg-white box-shadow b-b b-light" data-spy="affix" data-offset-top="1">
    <div class="container"> 
        <div class="navbar-header"> 
            <a href="<?= HOME ?>" class="navbar-brand">
                <img src="media/images/logo.png" class="m-r-sm">
                <span class="text-muted">KaribuSMS</span>
            </a>
            <button class="btn btn-link visible-xs" type="button" data-toggle="collapse" data-target=".navbar-collapse"> 
                <i class="fa fa-bars"></i> 
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"> 
                    <a href="<?= HOME ?>">Home</a> 
                </li>
                
                <li> 
                    <a href="<?= HOME ?>features">Features</a> 
                </li>
               
                <li> 
                    <a href="<?= HOME ?>karibusmspro">karibuSMS pro</a> 
                </li>
                <li> 
                    <a href="<?= HOME ?>shortcode">Shortcode</a> 
                </li>
                <li> 
                    <a href="<?= HOME ?>faq">FAQ</a> 
                </li>
<!--                <li> 
                    <a href="<?= HOME ?>help&sec=samples">Sample ads</a> 
                </li>-->
                 <li> 
                    <a href="<?= HOME ?>payment">Pricing</a> 
                </li>
                 <li> 
                    <a href="<?= HOME ?>help&sec=blog">Blog</a> 
                </li>
                <?php if (!isset($ses_user)) { ?>
                    <li> 
                        <div class="m-t-sm">
                            <a href="<?= HOME ?>" class="btn btn-link btn-sm">Sign in</a> 
                            <a href="<?= HOME ?>register" class="btn btn-sm btn-success m-l"><strong>Sign up</strong></a>
                        </div> 
                    </li> 
                <?php } ?>
            </ul> 
        </div> 
    </div> 
</header> <!-- / header -->
