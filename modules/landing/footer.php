<?php
/**
 *   karibu.com
 * * @author Ephraim Swilla <swillae1@gmail.com>
 */
?>
<footer id="footer">
    <div class="text-center padder"> <p> <small>KaribuSMS web application<br>&copy; <?=  date('Y')?></small> </p> </div>
</footer>