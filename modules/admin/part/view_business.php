
<div class="m-b-md"> <h3 class="m-b-none">Statistics Table</h3> </div> <div class="row"> 
     
     <div class="col-sm-12"> 
        
         <section class="panel panel-default"> 
             <?php
             $table=new data_table();
             $table->set_table_name('business');
             $table->set_table_fetch_where(array('id'=>$_GET['id']));
             $table->datatable=FALSE;
             $table->set_header_titles(array('','name','username','phone_number_valid','email','lastlogin','messaging_type','facebook','gcm_id','weblink','twitter','map'));
             $table->action_sms(array('pg'=>'admin','part'=>'sms_business'), 'view_business', TRUE);
             $table->display();
             ?>
         </section>
     </div>
     <div class="col-sm-12"> 
        
     </div>
 </div>
 