
<div class="modal-dialog">
    <div class="modal-content"> 
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Send Message</h4> 
        </div> <div class="modal-body">
            <div id="ajax_sms_result"></div>
            <section class="panel panel-default">
                <form> 
                    <textarea class="form-control no-border" onclick="$(this).css({'border': 'none'})" id="content_area" rows="3" placeholder="Say something and post to all businesses "></textarea> 
                </form>
                
            </section>
            
        </div> 
        
        <div class="modal-footer"> 
            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a> 
            <a href="#" class="btn btn-primary" onclick="javascript:send_sms_toallbusiness();">Send</a> 
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script>
    send_sms_toallbusiness = function() {
       var content = $('#content_area').val();
        if (content == '') {
            $('#content_area').css({"border": "1px solid red"});
            return false;
        }
        $('#ajax_sms_result').html(LOADER);
        $.getJSON(url, {pg: 'admin', process: 'send_sms_tobusiness', content: content}, function(data) {
            if (data.error == '') {
                //$('#ajax_sms_result').html(data.success);
                $('#ajax_sms_result').html(data.message);
            } else {
                $('#ajax_sms_result').html(data.error);
            }
            console.log(data);
        }).error(function(data) {
            $('#ajax_sms_result').html(data.error);
            console.log(data);
        });
        $('#content_area').val('');
    };
</script>