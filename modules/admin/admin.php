<?php

$businesses=  business::find_all();
        
?> 
<section class="vbox"> 
     <section class="scrollable padder"> 
	 <ul class="breadcrumb no-border no-radius b-b b-light pull-in"> 
             <li><a href="#"><i class="fa fa-home"></i> Home</a></li> <li class="active">Admin summary</li> </ul>
	 <div class="m-b-md"> <h3 class="m-b-none">karibuSMS</h3> <small>Customers summary</small> </div> 
	 <section class="panel panel-default">
	     <div class="row m-l-none m-r-none bg-light lter"> 
                 <div class="col-sm-6 col-md-3 padder-v b-r b-light"> 
                     <span class="fa-stack fa-2x pull-left m-r-sm">
                         <i class="fa fa-circle fa-stack-2x text-info"></i> 
                         <i class="fa fa-male fa-stack-1x text-white"></i>
                     </span> 
                     <a class="clear" href="#">
                         <span class="h3 block m-t-xs"><strong><?=  count($businesses);?></strong></span>
                         <small class="text-muted text-uc">Total businesses</small> </a> </div> 
                 <div class="col-sm-6 col-md-3 padder-v b-r b-light lt"> 
                     <span class="fa-stack fa-2x pull-left m-r-sm">
                         <i class="fa fa-circle fa-stack-2x text-warning"></i>
                         <i class="fa fa-phone fa-stack-1x text-white"></i>
                         <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#fff" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="2000" data-target="#bugs" data-update="3000" style="width: 50px; height: 50px; line-height: 50px;">
                             <canvas width="50" height="50"></canvas>
                         </span>
                         <?php 
                         $contacts=  subscriber::find_all();
                         ?>
                     </span> <a class="clear" href="#"> 
                         <span class="h3 block m-t-xs"><strong id="bugs"><?=  count($contacts);?></strong></span> 
                         <small class="text-muted text-uc">Customer contacts</small> </a>
                 </div> 
                 <div class="col-sm-6 col-md-3 padder-v b-r b-light"> 
                     <span class="fa-stack fa-2x pull-left m-r-sm"> 
                         <i class="fa fa-circle fa-stack-2x text-danger"></i> 
                         <i class="fa fa-envelope fa-stack-1x text-white"></i>
                         <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;">
                             <canvas width="50" height="50"></canvas>
                         </span> 
                     </span> 
                     <a class="clear" href="#"> 
                         <span class="h3 block m-t-xs"><strong id="firers"><?=  count(sms::find_all())?></strong></span> 
                         <small class="text-muted text-uc">SMS sent</small> 
                     </a> 
                 </div> 
                 <div class="col-sm-6 col-md-3 padder-v b-r b-light lt">
                     <span class="fa-stack fa-2x pull-left m-r-sm"> 
                         <i class="fa fa-circle fa-stack-2x icon-muted"></i> 
                         <i class="fa fa-retweet fa-stack-1x text-white"></i> 
                     </span> 
                     <a class="clear" href="#">
                         <span class="h3 block m-t-xs"><strong>0</strong></span> 
                         <small class="text-muted text-uc">Customer request</small> </a> </div> </div> 
	 </section> 
         <a class="btn btn-s-md btn-primary btn-rounded" href="<?= $AJAX ?>pg=admin&part=message&type=<?=$ses_user->messaging_type?>" 
                                       data-toggle="ajaxModal">
                                        <i class="fa fa-comment-o"></i> Send Message to All Businesses</a><br/>
         <div class="row" id="view_business"> 
             <div class="col-md-8">
                 <section class="panel panel-default">
                    
               <?php
               $table=new data_table();
               $table->set_table_name('business');
               $table->set_header_titles(array("",'name','phone_number'));
               $table->action_view(array('pg'=>'admin','part'=>'view_business'), 'view_business');
               $table->action_sms(array('pg'=>'admin','part'=>'sms_business'), 'view_business',TRUE);
               $table->display();
               ?>
                 </section> </div>
             <div class="col-md-4">
<!--                     <section class="panel panel-default"> 
                         <header class="panel-heading font-bold">Data graph</header> 
                         <div class="bg-light dk wrapper"> 
                             <span class="pull-right">Friday</span> 
                             <span class="h4">$540<br> <small class="text-muted">+1.05(2.15%)</small> </span>
                             <div class="text-center m-b-n m-t-sm"> 
                                 <div class="sparkline" data-type="line" data-height="65" data-width="100%" data-line-width="2" data-line-color="#dddddd" data-spot-color="#bbbbbb" data-fill-color="" data-highlight-line-color="#fff" data-spot-radius="3" data-resize="true" values="280,320,220,385,450,320,345,250,250,250,400,380"><canvas width="318" height="65" style="display: inline-block; width: 318px; height: 65px; vertical-align: top;"></canvas></div> 
                                 <div class="sparkline inline" data-type="bar" data-height="45" data-bar-width="6" data-bar-spacing="6" data-bar-color="#65bd77"><canvas width="150" height="45" style="display: inline-block; width: 150px; height: 45px; vertical-align: top;"></canvas></div>
                             </div>
                         </div> 
                         <div class="panel-body"> 
                             <div> <span class="text-muted">Total:</span> <span class="h3 block">$2500.00</span> </div>
                             <div class="line pull-in"></div> <div class="row m-t-sm">
                                 
                                 <div class="col-xs-4"> <small class="text-muted block">Market</small> <span>$1500.00</span> </div> <div class="col-xs-4"> <small class="text-muted block">Referal</small> <span>$600.00</span> </div> <div class="col-xs-4"> <small class="text-muted block">Affiliate</small> <span>$400.00</span> </div> </div> </div> 
                     </section> -->
             </div>
         </div> 
 
     </section>
 </section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> 