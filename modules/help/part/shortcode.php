<h3 class="panel-heading">Using our 15573 shortcode number</h3> 
<p class="padder">Our shortcode number is used to simplify business process. You can use the whole karibuSMS application by using shortcode without any need to login anywhere. </p>

<section class="panel panel-default">
    <h4 class="padder" style="padding: 10px">keyword used by people and customers</h4>
    <table class="table table-striped m-b-none text-sm"> 

        <thead> 
            <tr>
                <th>keyword</th> 
                <th>Uses</th>
                <th>Main user</th>
            </tr> 
        </thead>
        <tbody> 
            <tr>
                <td>KARIBU BUSINESSNAME</td> 
                <td>People write to subscribe to your business</td>
                <td class="text-right">People who wants to be connected with business
                </td>
            </tr> 
            <tr>
                <td>KARIBU UNSUBSCRIBE BUSINESSNAME</td> 
                <td>Customers write to unsubscribe to your business</td>
                <td class="text-right">Customers who are already connected with you
                </td>
            </tr>
        </tbody>
    </table>
    <h4 class="padder" style="padding: 10px">keyword used by business only</h4> 
    <table class="table table-striped m-b-none text-sm">
        <thead> 
            <tr>
                <th>keyword</th> 
                <th>Uses</th>
                <th>Main user</th>
            </tr> 
        </thead>
        <tbody>
            <tr>
                <td>KARIBU REGISTER BUSINESSNAME</td> 
                <td>First time register your BUSINESSNAME</td>
                <td class="text-right">karibuSMS pro messaging business
                </td>
            </tr>
            <tr>
                <td>KARIBU NUMBER 07P4XYZYXS, 065XZPSDY.......</td> 
                <td>Upload new customer contacts in your profile</td>
                <td class="text-right">karibuSMS pro and karibuSMS businesses
                </td>
            </tr>
            <tr>
                <td>KARIBU MPESA RECEIPT_NUMBER</td> 
                <td>Complete MPESA payment by sending receipt number codes (e.g BF24SU324 confirm....)</td>
                <td class="text-right">karibuSMS pro and karibuSMS businesses
                </td>
            </tr>
            <tr>
                <td>KARIBU TIGOPESA RECEIPT_NUMBER</td> 
                <td>Complete TIGOPESA payment by sending receipt number codes (e.g ..your TxnId id is RC140509.2237.J08261 confirm....)</td>
                <td class="text-right">karibuSMS pro and karibuSMS businesses
                </td>
            </tr>
        </tbody> 
    </table> 
      <h4 class="padder" style="padding: 10px">NB: Sending one SMS with shortcode will cost you a little cost of Tsh 150 per SMS</h4> 
</section>