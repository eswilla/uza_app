<div class="caption wrapper-lg"> 
    <h2 class="post-title"><a href="#">What is karibuSMS pro?</a></h2> 
    <div class="post-sum"> <p><br/>
            karibuSMS pro is the messaging service which do not require a business to have an android phone in order to send SMS.
        </p>
        <h3>How it works</h3>
        <p>karibuSMS is pro works in very simplest way as follows</p>
        <ol>
            <li>If you don't have an account, create you karibuSMS account <a href="<?= HOME ?>register" class="label label-info">here</a> </li>
            <li>After sign in, go to settings tab in the left side of your profile, click the tab messaging type and tick to select, karibuSMS pro</li>
            <li>Return to your profile and you will be able to send SMS to your customers using internet messaging.</li>
        </ol>
    </div>

    <div class="bg-white b-t"> 
        <p></p>
        <h3>Cost plan</h3>
        <p>A pro messaging you can use either bundle SMS payments or buy per SMS. Internet messaging is high costly due to high charge rate we receive from mobile network providers</p>
        <ul>
            <?php include_once 'modules/payment/part/packages.php'; ?>
        </ul>
    </div> 
    <div>
        <div class="b-t b-light"> 
            <div class="container m-t-xl"> 
                <div class="row"> <div class="col-sm-7"> 
                        <h2 class="font-thin m-b-lg">Differences between the two</h2> 
                 <section class="panel panel-default"> 
                  <table class="table table-striped m-b-none text-sm">
                      <thead> 
                          <tr> 
                              <th>Feature</th> 
                              <th>karibuSMS</th> 
                              <th>karibuSMS pro</th> 
                          </tr> </thead>
                      <tbody> 
                          <tr> 
                              <td>Use of shortcode (15573)</td> 
                              <td>Yes</td>
                              <td class="text-success"> Yes</td>
                          </tr>
                           <tr> 
                              <td>Sender id</td> 
                              <td>phone number only</td>
                              <td class="text-success">Phone number or business name</td>
                          </tr>
                           <tr> 
                              <td>Cost plan</td> 
                              <td>Pay per month</td>
                              <td class="text-success">Pay per SMS or monthly bundle</td>
                          </tr>
                    </tbody> 
                  </table> 
                 </section>
                    </div> 
                </div> 

            </div>
        </div>
    </div>
    <div class="line line-lg"></div>
</div>