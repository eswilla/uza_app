<?php
require_once 'modules/landing/banner/feature_banner.php';
?>  
<section id="content">
    <div class="bg-dark lt"> 
        <div class="container">
            <div class="m-b-lg m-t-lg"> 
                <h3 class="m-b-none"><?= isset($page_part) ? ucwords($page_part) : 'BLOG' ?></h3> 
                <small class="text-muted"><?=
                    isset($page_part) ? ucwords($page_part) . ' Article' :
                    'We give you the latest news occurs in karibuSMS'
                    ?></small>
            </div>
        </div> 
    </div>
    <div class="bg-white b-b b-light"> 
        <div class="container"> 
            <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm"> 
                <li><a href="<?= HOME ?>">Home</a></li> 
                <li class="active">Blog</li>
            </ul> 
        </div> 
    </div>
    <?php
    if (isset($_GET['id'])) {
        $title = array('id' => $_GET['id']);
    } else if (isset($_GET['category'])) {
        $title = array('category' => $_GET['category']);
    } else {
        $title = array('id' => 1);
    }
    $blog_info = blog::find_where($title);
    $blog = array_shift($blog_info);
    ?>
    <div class="container m-t-lg m-b-lg">
        <div class="row"> 
            <div class="col-sm-9"> 
                <div class="blog-post"> 
                    <div class="post-item"> 
                        <div class="post-media"> 
                            <section class="panel  dk m-b-none"> 

                                <div class=" auto slide " id="c-fade">
                                    <?= $blog->photo !=''? "<img src='".$blog->photo."' width='600' height='300'/>":''?>
                                </div>
                            </section>
                        </div>
                        <div class="caption wrapper-lg">
                            <h2 class="post-title"><a href="#"><?= $blog->title ?></a></h2>
                            <div class="post-sum"> 
                                <p><?= $blog->content ?></p> 
                            </div> <div class="line line-lg"></div> 
                            <div class="text-muted"> 
                                <i class="fa fa-user icon-muted"></i> by <a href="#" class="m-r-sm">PR manager</a> 
                                <i class="fa fa-clock-o icon-muted"></i> <?= date('M d, Y', strtotime($blog->time)) ?>
                                <?php
                                $coments = comment::find_where(array('blog_id' => $blog->id));
                                if (!empty($coments)) {
                                    ?>
                                    <a href="#" class="m-l-sm">
                                        <i class="fa fa-comment-o icon-muted"></i> <?= count($coments) ?> comments</a> 
                                <?php } ?>
                            </div> </div> 
                    </div> 
                </div>
                <div class="fb-comments" data-href="http://karibusms.com/help&amp;sec=blog&id=<?=$blog->id?>" data-numposts="5" data-colorscheme="light"></div>
                <?php
                if (!empty($coments)) {
                    foreach ($coments as $comment) {
                        ?>
                        <h4 class="m-t-lg m-b"><?= count($coments) ?> Comments</h4> 
                        <section class="comment-list block"> 
                            <article id="comment-id-1" class="comment-item"> 
                                <a class="pull-left thumb-sm">
                                    <img src="media/images/avatar_default.jpg" class="img-rounded"> </a> 
                                <section class="comment-body m-b"> 
                                    <header> 
                                        <a href="#"><strong><?=$comment->name?></strong></a>
                                        <label class="label bg-info m-l-xs"></label> 
                                        <span class="text-muted text-xs block m-t-xs"><?=$input->make_time_ago($comment->time)?></span> 
                                    </header>
                                    <div class="m-t-sm"><?=$comment->content?></div>
                                </section> 
                            </article> <!-- .comment-reply -->                
                        </section> 
                    <?php }
                }
               
                ?>
                        <div id="ajax_blog_result"></div>
                <h4 class="m-t-lg m-b">Leave a comment</h4> 
                <form  id="blog"> 
                    <div class="form-group pull-in clearfix"> <div class="col-sm-6"> <label>Your name</label> <input type="text" class="form-control" name="name" placeholder="Name"> </div> <div class="col-sm-6"> <label>Email</label> <input type="email" class="form-control" name="email" placeholder="Enter email"> </div> </div>
                    <div class="form-group"> <label>Comment</label> <textarea name="content" class="form-control" rows="5" placeholder="Type your comment"></textarea> </div>
                    <input type="hidden" name="blog_id" value="<?=$blog->id?>"/>
                    <div class="form-group"> 
                        <button type="button" onmousedown="javascript:send_comment()" class="btn btn-success">Submit comment</button> </div> 
                </form> 
            </div> 
            <div class="col-sm-3"> 
                <h5 class="font-semibold">lets get more connected</h5>
                <div class="line line-dashed"></div> 
                <div class="m-t-sm m-b-lg"> 

                    <a  title="twitter" href="https://twitter.com/karibuSMS" class="twitter-follow-button" data-show-count="true" data-lang="en">Follow @karibuSMS</a>

                    <a href="#" title="Facebook">
                        <div class="fb-like" data-href="http://karibusms.com" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>

                    </a> 
                    <!-- Place this tag in your head or just before your close body tag. -->
                    <script src="https://apis.google.com/js/platform.js" async defer></script>
                    <link rel="canonical" href="http://www.karibusms.com" />
                    <!-- Place this tag where you want the +1 button to render. -->
                    <div class="g-plusone" data-annotation="inline" data-width="300"></div> 
                </div> 
                <h5 class="font-semibold">Recent Tweets</h5> 
                <div class="line line-dashed"></div> 
                <div class="clear m-b"> 
                    <div class="span3 scrollbar">
                        <a class="twitter-timeline" href="https://twitter.com/KaribuSMS" data-widget-id="517442897914757122">Tweets by @KaribuSMS</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        <script>
                        function send_comment(){
    $('#ajax_blog_result').html(LOADER);
var formdata=$('#blog').serialize();
$.get( url, {pg:"general",process:"insert",formdata:formdata },function(data){
     $('#ajax_blog_result').html(data);
});
}        
                        </script>

                    </div>
                </div> 

                <div class="line line-dashed"></div>

                <h5 class="font-semibold">Recent Posts</h5> <div class="line line-dashed"></div> 
                <div>
                    <?php
                    $recent_posts = blog::find_by_sql("SELECT * FROM karibu_blog ORDER BY RAND() LIMIT 4");
                    foreach ($recent_posts as $post) {
                        ?>
                        <article class="media"> 
                            <a class="pull-left thumb thumb-wrapper m-t-xs"> 
    <?= $post->photo !=''? "<img src='".$post->photo."' width='200' height='200'/>":''?></a>
                            <div class="media-body">
                                <a href="<?=HOME?>help&sec=blog&blog&id=<?= $post->id ?>" class="font-semibold"><?= $post->title ?></a>
                                <div class="text-xs block m-t-xs">
                                    <a href="#">PR manager</a> 
                        <?= date('M d, Y', strtotime($post->time)); ?></div>
                            </div> 
                        </article> 
                        <div class="line"></div> 
<?php } ?>
                </div> 
            </div> 
        </div> 
    </div></section>
<?php
require_once 'modules/landing/feature_footer.php';
//js_media('landing');
 js_media('jquery.form');
?>
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=526011487520671&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = "//platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, "script", "twitter-wjs");</script>