<?php
require_once 'modules/landing/banner/feature_banner.php';
?>  

<section id="content">
    <div class="bg-dark lt"> 
        <div class="container">
            <div class="m-b-lg m-t-lg"> <h3 class="m-b-none"><?= isset($page_part) ? ucwords($page_part) : 'FAQ' ?></h3> 
                <small class="text-muted"><?= isset($page_part) ? ucwords($page_part) . ' plan' : 'Frequently asked question' ?></small> </div> </div> </div>

    <div class="bg-white b-b b-light"> 
        <div class="container"> 
            <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
                <li><a href="<?= HOME ?>">Home</a></li> <li class="active"><?= isset($page_part) ? ucwords($page_part) : 'FAQ' ?></li> </ul> </div> </div>

    <div class="container m-t-lg m-b-lg">
        <div class="row">
            <div class="col-sm-9">

                <div class="blog-post"> 
                    <div class="post-item">
                        <?php
                        if (isset($page_part)) {
                            include_once 'modules/help/part/' . $page_part . '.php';
                        } else {
                            ?>
                            <div class="caption wrapper-lg"> 
                                <h2 class="post-title">
                                    <a href="#">Q&A to help you easily use karibuSMS</a>
                                </h2> 
                                <div class="post-sum">
                                    <div class="panel-group m-b" id="accordion2"> 
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="accordion-toggle collapsed" 
                                                   data-toggle="collapse" data-parent="#accordion2" 
                                                   href="#collapseOne">#1 How can I get started?  </a> 
                                            </div> 
                                            <div id="collapseOne" class="panel-collapse collapse" style="height: 0px;"> 
                                                <div class="panel-body text-sm"> 
                                                    <ol>
                                                        <li>Download an android application <a href="http://goo.gl/msamgD" target="_blank" class="label label-info">here in google market</a></li>
                                                        <li>After installation, register your business name, phone number and password</li>
                                                        <li>Signin the application and upload your customer contacts if you have in the application (there is an interface to do that)</li>
                                                        <li>Now you can start to send SMS in the interface for sending SMS and the message will go to all contacts in your karibuSMS profile</li>
                                                    </ol>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" 
                                                   data-parent="#accordion2" href="#collapseTwo"> #2  What if I don't have an android 
                                                    phone and I wan't to use karibuSMS? can I be able to use karibuSMS?</a>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse in" style="height: auto;"> 
                                                <div class="panel-body text-sm">
                                                    YES, You can enjoy karibuSMS service even if you don't have android phone. These are steps.

                                                    <ol>
                                                        <ul><h4>KaribuSMS has two messaging services</h4></ul>
                                                        <li>karibuSMS smart phone messaging version(karibuSMS)</li>
                                                        <li>karibuSMS internet messaging version(karibuSMS pro). <a href="<?= HOME ?>karibusmspro" class="label label-info">Click here </a>on the instruction to use this service</li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <a class="accordion-toggle collapsed" 
                                                   data-toggle="collapse" data-parent="#accordion2"
                                                   href="#collapseThree">#3 How subscription works? Do my customers need smart phone to receive SMS? </a>
                                            </div> 
                                            <div id="collapseThree" class="panel-collapse collapse"> 
                                                <div class="panel-body text-sm"> 
                                                    <ul> No, customer with any kind of mobile phone can receive SMS you send.
                                                        <br/><br/>
                                                        People can subscribe to know your business by 
                                                        sending normal SMS with keyword KARIBU YOURBUSINESSNAME to 15573, 
                                                        and they will get connected. Once you send SMS, it will be received to all customers (those who subscribe and those who you upload contacts)
                                                    </ul>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse"
                                                   data-parent="#accordion2" href="#collapseFour">#4 What is the relation between www.karibusms.com and karibuSMS android app ?</a> </div> 
                                            <div id="collapseFour" class="panel-collapse collapse"> 
                                                <div class="panel-body text-sm"> 
                                                    karibuSMS can be accessed via android app or web based in www.karibusms.com. This is one platform. www.karibusms.com give business
                                                    more easily way to manage their profile includes changing of settings, upload contacts by excel file, make payment with paypal or credit cards and much more.
                                                    <br/>
                                                    Your customers receive SMS having a link to your online profile page in www.karibusms.com where they can see much of your products and services you have. In web based you have more control of your profile than in your phone.

                                                </div> 

                                            </div>
                                        </div>
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse"
                                                   data-parent="#accordion2" href="#collapseFive">#5 Can my message posted in karibuSMS be posted in my facebook page also ?</a> </div> 
                                            <div id="collapseFive" class="panel-collapse collapse"> 
                                                <div class="panel-body text-sm"> 
                                                    Yes, you can link karibuSMS with your facebook page so that when you send SMS, it will be sent to all your customers in your profile in their mobile phone as SMS and
                                                    the same message to be posted in your facebook page</div> 

                                            </div>
                                        </div>
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse"
                                                   data-parent="#accordion2" href="#collapseFive">#6 Is karibuSMS works well in all network providers available in my country?</a> </div> 
                                            <div id="collapseFive" class="panel-collapse collapse"> 
                                                <div class="panel-body text-sm"> 
                                                    Yes, karibuSMS works well in all network providers available.</div> 

                                            </div>
                                        </div>
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse"
                                                   data-parent="#accordion2" href="#collapseFive">#7 Do I need credit balance to send SMS to my customers?</a> </div> 
                                            <div id="collapseFive" class="panel-collapse collapse"> 
                                                <div class="panel-body text-sm"> 
                                                    If you use karibuSMS, your phone should have SMS to send. karibuSMS access SMS you have in your phone. However, if you use karibuSMS pro, you don't have SMS in your phone to send SMS.</div> 

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="line line-lg"></div>
                                <div class="text-muted">

                                </div>
                            </div>
                        <?php } ?>
                    </div> 

                </div> 

                <h4 class="m-t-lg m-b">Comments</h4> 

                <h4 class="m-t-lg m-b">Leave a comment</h4> 
                <div class="fb-comments" data-href="http://www.karibusms.com" data-numposts="5" data-colorscheme="light"></div>
            </div> 
            <div class="col-sm-3">

                <h5 class="font-semibold">connect</h5>
                <div class="line line-dashed"></div> 
                <div class="m-t-sm m-b-lg">


                    <div class="fb-like" data-href="https://www.karibusms.com" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>


                </div>
                <div class="line line-dashed"></div> 

                <h4 class="font-semibold">Read more on</h4>
                <div class="line line-dashed"></div> 
                <ul class="list-unstyled"> 
                    <li> <a href="<?= HOME ?>karibusmspro" class="dk"> 
                            <span class="badge pull-right"></span> karibuSMS pro messaging </a> 
                    </li>
                    <li class="line"></li>
                    <li> <a href="<?= HOME ?>shortcode">
                            <span class="badge pull-right"></span>Shortcode number uses & benefits</a> </li>
                    <li class="line"></li> 
                </ul>

            </div> 
        </div> 
    </div>
</section>
<?php
require_once 'modules/landing/feature_footer.php';

js_media('appear/jquery.appear');
js_media('scroll/smoothscroll');
js_media('landing');
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=278267052336362&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>