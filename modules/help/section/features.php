<?php
require_once 'modules/landing/banner/feature_banner.php';
?>  

<section id="content">
    <div class="bg-dark lt">
        <div class="container"> 
            <div class="m-b-lg m-t-lg"> <h3 class="m-b-none">Features</h3> 
                <small class="text-muted">What you need know</small>
            </div> 
        </div>
    </div> 
    <div class="bg-white b-b b-light">
        <div class="container">
            <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
                <li><a href="<?= HOME ?>">Home</a></li> <li class="active">Features</li>
            </ul>
        </div> 
    </div> 
    <div> <div class="container m-t-xl">
            <div class="row"> 
                <div class="col-sm-7"> 
                    <h2 class="font-thin m-b-lg">What is
                        <span class="text-primary">karibuSMS </span>
                        <span class="text-primary"></span> 
                        <span class="text-primary"></span>
                    </h2>
                    <p class="h4 m-b-lg l-h-1x">
                        Is the advertising and promotion, SMS platform, that keeps you connected to your  customers  with online profile and by sending SMS notifications easily to customers inform them about your services/products.
                    <div class="font-bold m-t-xl m-b-xl"><span class="fa-2x icon-muted">Who can use</span> </div>
                    </p>
                    <p>Used by companies, organizations, supermarkets and other individuals to</p>
                    <div class="row m-b-xl"> 
                        <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Inform customers with new offers</div> 
                        <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Send reminder SMS for events/appointments</div>
                        <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Notify them for new stock or service</div>
                        <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Create promotions SMS </div>
                        <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>And more as you need....</div>
                    </div> 
                    <div class="row m-b-xl">
                         <div class="font-bold m-t-xl m-b-xl"><span class="fa-2x icon-muted">Why you should use karibuSMS</span> </div>
                        <div class="col-sm-6"> <i class="fa fa-fw fa-angle-right"></i>Register your business and have an online profile for your business, </div>
                         <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i> Upload in your profile, products and services you have for customers to view  </div>
                         <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>send SMS to group of customers at once  </div>
                         <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Customers receive SMS with a link  to direct them to your profile page for them to view your products more </div>
                         <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i> Customer subscribe to your business with keyword KARIBU BUSINESSNAME to 15573 </div>
                    </div>
                </div> 
                <div class="col-sm-5 text-center"> 
                    <section class="panel bg-dark inline aside-md no-border device phone animated fadeInRightBig">
                        <header class="panel-heading text-center">
                            <i class="fa fa-minus fa-2x icon-muted m-b-n-xs block"></i>
                        </header> <div class="m-l-xs m-r-xs"> 
                            <div class="carousel auto slide" id="c-fade" data-interval="3000">
                                <div class="carousel-inner"> 
                                    <div class="item active text-center"> 
                                        <img src="media/images/phone-2.png" class="img-full" > 
                                    </div> 
                                    <div class="item text-center"> 
                                        <img src="media/images/phone-1.png" class="img-full" > 
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                        <footer class="bg-dark text-center panel-footer no-border"> <i class="fa fa-circle icon-muted fa-lg"></i> </footer>
                    </section>
                </div> 
            </div> 
        </div> 
    </div> 

    <div class="bg-white b-t b-light"> 
        <div class="container"> 
            <div class="row m-t-xl m-b-xl"> 
                <div class="col-sm-5 text-center clearfix m-t-xl" data-ride="animated" data-animation="fadeInLeftBig"> 
                    <div class="h2 font-bold m-t-xl m-b-xl">
                        <span class="fa-2x icon-muted"><a href="#">{KARIBU}</a></span>
                    </div> 
                </div> 
                <div class="col-sm-7"> 
                    <h2 class="font-thin m-b-lg">How it works</h2> 
                    <p class="h4 m-b-lg l-h-1x">
                    <ol>
                        <li>Using your smart mobile phone( A phone with touch screen), visit  <span class="label label-success">www.karibusms.com</span> and click the link to download the application or 
                            <a href="https://play.google.com/store/apps/details?id=com.karibusms.smart&hl=en" target="_blank" class="label label-success">click here</a></li>
                        <li>Register your business with your business name and phone number then you are done</li>
               
                    </ol>
                    <p>Don't you have a android smart phone yet? We have a plan for you, <a href="<?=HOME?>karibusmspro" class="label label-success">just click here</a></p>
                    <p class="m-t-xl m-b-xl h4"><i class="fa fa-quote-left fa-fw fa-1x icon-muted"></i> Customers will
                        subscribe to your business by send SMS 
                        with keyword KARIBU BUSINESSNAME to <?= $SUBSCRIPTION_NUMBER ?></p>
                    </p>
                    <p class="m-b-xl">Then:<br/>
                        You can send SMS to your customers by either
                        <br/>
                    <ul>
                        <li>Open the karibuSMS application in your phone and send a message</li>
                        <ul>OR</ul>
                        <li>Login in your account on <a href="<?= HOME ?>">www.karibusms.com</a> and compose SMS to send to all</li>
                        <ul>OR</ul>
                        <li>Use the phone number you register your business with here, start with keyword KARIBU followed by 
                            SMS you would like your customers to read and send it to <?= $SUBSCRIPTION_NUMBER ?></li>
                    </ul>
                    </p> 
                    <p class="m-t-xl m-b-xl h4"><i class="fa fa-quote-left fa-fw fa-1x icon-muted"></i> We make very simple for you</p>
                    <small><a href="<?= HOME ?>register">If you don't want to use smart phone, we have a plan for internet message for you. If you want this plan, <a href="<?=HOME?>karibusmspro" class="label label-success">click here</a></small>
                </div>

            </div> 
        </div> 
    </div>

    <div class="b-t b-light"> 
        <div class="container m-t-xl"> 
            <div class="row">
                <div class="col-sm-7"> 
                    <h2 class="font-thin m-b-lg">How to access the platform</h2> 
                    <p class="h4 m-b-lg l-h-1x">
                    You can access karibuSMS with any internet supporting device as long as your SIM card is in your smart phone  <a href="<?= HOME ?>help&sec=samples" style="color:#65bd77;">feature phone</a> to communicate with your customers
                    </p> 
                    <ul>
                        <p><i class="fa fa-fw fa-angle-right"></i>mobile phone(smart phone)</p>
                        <p><i class="fa fa-fw fa-angle-right"></i>Tablets</p>
                        <p><i class="fa fa-fw fa-angle-right"></i>Computer</p>
                    </ul>

                    <p class="m-b-xl">
                        <a href="<?= HOME ?>register" class="btn btn-sm btn-primary font-bold">Start it now</a> 
                    </p> 
                    <p class="clearfix">&nbsp;</p>
                    <div class="row m-b-xl">
                        <div class="col-xs-2"><i class="fa fa-desktop fa-4x icon-muted"></i></div>
                        <div class="col-xs-2"><i class="fa fa-plus icon-muted m-t"></i></div>
                        <div class="col-xs-2"><i class="fa fa-tablet fa-4x icon-muted"></i></div>
                        <div class="col-xs-2"><i class="fa fa-plus icon-muted m-t"></i></div> 
                        <div class="col-xs-2"><i class="fa fa-mobile fa-4x icon-muted"></i></div>
                    </div>
                </div> 
                <div class="col-sm-5 text-center" data-ride="animated" data-animation="fadeInUp" > 
                    <section class="panel bg-dark m-t-lg m-r-n-lg m-b-n-lg no-border device animated fadeInUp"> 
                        <header class="panel-heading text-left"> 
                            <i class="fa fa-circle fa-fw icon-muted"></i> 
                            <i class="fa fa-circle fa-fw icon-muted"></i> 
                            <i class="fa fa-circle fa-fw icon-muted"></i> 
                        </header> 
                        <img src="media/images/main.png" class="img-full" >
                    </section>
                </div>
            </div>
        </div> 
    </div> 

    <div class="bg-white b-t b-light pos-rlt">
        <div class="container"> 
            <div class="row m-t-xl m-b-xl"> 
                <div class="col-sm-5 text-center clearfix m-t-xl" data-ride="animated" data-animation="fadeInLeftBig"> 
                    <div class="h3 font-bold m-t-xl m-b-xl">
                        <img src="media/images/mpesa.jpg" width="40%" title="M-PESA payment" alt="M-PESA payment"/>
                        <img src="media/images/tigopesa.jpg" width="40%" title="Tigopesa payment" alt="Tigopesa payment"/>
                        <img src="media/images/paypal.jpg" width="40%" title="Paypal and bank payment" alt="Paypal and bank payment"/> 

                    </div> 
                </div> 
                <div class="col-sm-7">
                    <h2 class="font-thin m-b-lg">You can pay monthly fee with Method You Like</h2> 
                    <p class="h4 m-b-lg l-h-1x">
                        You can use either. 
                        <br/><br/>

                    </p> 
                    <ul class="m-b-xl fa-ul"> 
                        <li><i class="fa fa-li fa-check text-muted"></i>Mobile Payments (M-pesa, Tigo-Pesa)</li>
                        <li><i class="fa fa-li fa-check text-muted"></i>Credit Cards (using PayPal or Bank Cards)</li> 
                        <li><i class="fa fa-li fa-check text-muted"></i>Manual Payment</li> 
                    </ul> 
                </div> </div>
        </div> 
    </div> 

    <div class="b-t b-light"> 
        <div class="container m-t-xl"> 
            <div class="row"> 
                <div class="col-sm-7"> 
                    <h2 class="font-thin m-b-lg">Secure platform, Fast, Million+ User Support</h2> 
                    <p class="h4 m-b-lg l-h-1x"> 
                        Developed in high security, the application is simple to use, fast and
                        allow to send bulk SMS to your customers in minimum time.
                        <br/>
                    </p> 
                    <p class="m-b-xl"> <a href="<?= HOME ?>register" class="btn btn-dark font-bold">Get started now..!</a> </p> 
                </div> 
                <div class="col-sm-5 text-center" data-ride="animated" data-animation="fadeInRightBig" > 
                    <section class="panel bg-light m-t-lg m-r-n-lg m-b-n-lg no-border device animated fadeInUp">
                        <header class="panel-heading text-left"> 
                            <i class="fa fa-circle fa-fw icon-muted"></i>
                            <i class="fa fa-circle fa-fw icon-muted"></i> 
                            <i class="fa fa-circle fa-fw icon-muted"></i> 
                        </header> 
                        <img src="media/images/app.png" class="img-full" >
                    </section> 
                </div> 
            </div>
        </div> 
    </div> 

    <div class="b-t b-light pos-rlt bg-white"> <div class="container"> 
            <p class="m-t-xl m-b-xl"> </p>
        </div> 
    </div>
</section> <!-- footer --> 

<?php
require_once 'modules/landing/feature_footer.php';

js_media('appear/jquery.appear');
js_media('scroll/smoothscroll');
js_media('landing');
?>
