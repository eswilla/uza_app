<div class="modal-dialog" >
    <div class="modal-content">
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal">&times;</button> 
            <h4 class="modal-title">Give us your feedback</h4>
        </div> 
        <div class="modal-body"> 
	    <form data-validate="parsley"> 
		<section class="panel panel-default"> 
		    <header class="panel-heading"> 
			<span class="h4">Let us know about you</span>
		    </header> 
		    <div id="ajax_results2"></div>
		    <div class="panel-body"> 
			<div class="form-group ">
			    <form action="" method="get">
				<div class="input-group"> 
				    <input type="text" name="email" id="contact_email" class="form-control"  placeholder="Your email"/>
				</div>
				<textarea class="form-control"  id="comments_area"rows="6" data-minwords="6" data-required="true" placeholder="Describe shortly about you and what kind of service you want"></textarea>  

			    </form>
			</div> 
		    </div> 
		    <footer class="panel-footer text-right bg-light lter"> 
			<button type="button" class="btn btn-success btn-s-xs sett" onclick="get_send({pg: 'help', process: 'contact_us', email: $('#contact_email').val(), content: $('#comments_area').val()}, 'ajax_results2')">Submit</button>
		    </footer> 
		</section> 
	    </form>
	</div>
        <div class="modal-footer"> 
            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a> 
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->