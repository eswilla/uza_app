<?php
require_once 'modules/landing/banner/feature_banner.php';
?>  

<section id="content"> 
    <div class="bg-primary dk"> 
        <div class="text-center wrapper"> 
            <div class="m-t-xl m-b-xl"> 
                <div class="text-uc h1 font-bold inline">
                    <div class="pull-left m-r-sm text-white">KaribuSMS 
                        <span class="font-thin text-muted">sample</span>
                    </div> 
                    <div class="carousel slide carousel-fade inline auto aside text-left pull-left pull-none-xs" data-interval="2000"> 
                        <div class="carousel-inner"> 
                            <div class="item active text-warning">Flyer </div> 
                            <div class="item text-dark">Brochure</div>
                            <div class="item">Banner</div> 
                        </div> 
                    </div> 
                </div> 
                <div class="h4 text-muted m-t-sm">These are sample flyer instructions to help you get started easily</div>
            </div> 

            <p class="text-center m-b-xl"> <a href="<?= HOME ?>media/doc/market/readme.pdf" 
                                              target="_blank" class="btn btn-lg btn-dark m-sm">Download it</a> 
                <a href="<?= HOME ?>help&sec=preview&file=readmefront.jpg" target="_blank" class="btn btn-lg btn-warning b-white bg-empty m-sm">Live Preview</a>
            </p> 
        </div>
        <div class="padder"> 
            <div class="hbox"> 
                <aside class="col-md-3 v-bottom text-right"> 
                    <div class="hidden-sm hidden-xs">
                        <section class="panel bg-dark inline m-b-n-lg m-r-n-lg aside-sm no-border device phone 
                                 animated fadeInLeftBig"> 
                            <header class="panel-heading text-center"> 
                                <i class="fa fa-minus fa-2x m-b-n-xs block"></i> 
                            </header> 
                            <div> 
                                <div class="m-l-xs m-r-xs">
                                    <img src="media/images/phone-2.png" class="img-full" > 
                                </div>
                            </div> 
                        </section> 
                    </div> 
                </aside> 
                <aside class="col-sm-10 col-md-6"> 
                    <section class="panel bg-dark m-b-n-lg no-border device animated fadeInUp"> 
                        <header class="panel-heading text-left"> 
                            <i class="fa fa-circle fa-fw"></i> 
                            <i class="fa fa-circle fa-fw"></i> 
                            <i class="fa fa-circle fa-fw"></i> 
                        </header>
                        <img src="media/doc/market/readmefront.jpg" class="img-full" >
                    </section>

                </aside> 
                <aside class="col-md-3 v-bottom"> 
                    <div class="hidden-sm hidden-xs">
                        <section class="panel bg-light m-b-n-lg m-l-n-lg aside no-border device phone animated fadeInRightBig"> 
                            <header class="panel-heading text-center"> <i class="fa fa-minus fa-2x text-white m-b-n-xs block"></i>
                            </header> <div class=""> <div class="m-l-xs m-r-xs">
                                    <img src="media/images/phone-1.png" class="img-full" > 
                                </div> 
                            </div>
                        </section> 
                    </div> 
                </aside> 
            </div> 
        </div> 
        <div class="dker pos-rlt">
            <div class="container wrapper"> 
                <div class="m-t-lg m-b-lg text-center">You can download these samples or print. They provide step by step simple ways for best interaction with the application</div> 
            </div> 
        </div> 
    </div> 
    <div id="about"> 
        <div class="container">
            <div class="m-t-xl m-b-xl text-center wrapper"> 
                <h3>Continuation of instruction that we recommend you to download for easy reviewing </h3> 
                <p class="text-muted">This flyer is important to easily understand how this application work.</p> 
            </div> 
            <div class="row m-t-xl m-b-xl text-center">
                <section class="panel  animated fadeInUp"> 

                    <img src="media/doc/market/readmeback.jpg" class="img-full" >
                </section>

            </div> 
            <div class="m-t-xl m-b-xl text-center wrapper">
                <p class="h5">karibuSMS will help you
                    <span class="text-primary">To easily notify your customers about services and products you have</span>,
                    <span class="text-primary">Gain many number of customers with SMS subscription</span>,
                    <span class="text-primary">Increase customer retentions</span> and much more... </p>
            </div> </div> </div> <div id="responsive" class="bg-dark"> 
        <div class="text-center"> 
            <div class="container"> 
                <div class="m-t-xl m-b-xl wrapper"> 
                    <h3 class="text-white">Sample banner that you can put in your business to inform your customers for SMS subscription</h3> 
                    <p>You can print this banner after downloading it. <br>It is free,
                        <a href="<?= HOME ?>media/doc/market/banner.pdf"><span class="text-primary text-ul">Click here to download</span></a> a pdf file.</p>
                </div> 
                <div class="row m-t-xl m-b-xl"> 


                    <section class="panel bg-dark m-b-n-lg no-border device animated fadeInUp"> 
                        <img src="media/doc/market/banner.jpg" style="opacity: 0.9;" class="img-full" >
                    </section>
                    <div class="m-t-xl m-b-xl wrapper"> 
                        <h3 class="text-white">Put your businessname in ............place</h3> 
                        <p>So it will read <br>
                        <span class="text-primary">KARIBU YOURBUSINESSNAME</span></p>
               
                    </div>
                </div>
            </div> 
        </div> 
    </div> <div id="newsletter" class="bg-white clearfix wrapper-lg"> 
        <div class="container text-center m-t-xl m-b-xl" data-ride="animated" data-animation="fadeIn">
            <h2>Newsletter</h2>
            <p>Wishing to be the first to know? Subscribe to our newsletter box</p> 
            <form class="form-inline m-t-xl m-b-xl" onsubmit="return false"> <div class="form-group"> 
                    <input class="form-control input-lg" id="subscribe_email" placeholder="Your email" data-ride="animated"
                           data-animation="fadeInLeftBig" data-delay="300"> 
                </div> 
                <button type="button" class="btn btn-default btn-lg" data-ride="animated"
                        data-animation="fadeInRightBig" data-delay="600" onclick="javascript: subscribe();">Subscribe</button> 
            </form> 
        </div>
    </div>
</section>
<?php
require_once 'modules/landing/feature_footer.php';
js_media('appear/jquery.appear');
js_media('scroll/smoothscroll');
js_media('landing');
?>
<script type="text/javascript">
    subscribe = function() {
        var email = $('#subscribe_email').val();
        if (email == '') {
            alert('Please fill your email first');
        } else {
            $.get(url, {pg: 'general', process: 'subscribe', email: email}, function(data) {
                alert(data);
            });
        }
    };
</script>
