<?php

/**
 * Description of karibu
 *
 * @author Ephraim Swilla <swillae1@gmail.com>
 */
class karibu {

    static $paid;
    static $try;
    static $days_remain;
    static $max_contacts;
    static $max_sms;
    static $allow_location;
    static $allow_social_page;
    static $allow_shortcode;
    static $allow_file_upload;
    static $max_group;
    static $max_video;
    static $max_image = 5;
    static $access = array(
    );
    static $pro = array(
    );

    static function check_status() {
        global $ses_user;

        //this algorithm will change for feature, I just write it in hurry
        if (empty($ses_user)) {
            self::$max_group = 1500000;
            self::$max_image = 100000;
            self::$max_contacts = 1000000;
            self::$max_sms = 10000;
            self::$max_video = 70000;

            self::$allow_social_page = 1;
            self::$allow_shortcode = 1;
            self::$allow_location = 1;
            return FALSE;
        }
        if ($ses_user->messaging_type != 1) {
            $bp = self::check_bundle();
            if ($bp == 0) {
                echo "<span class=\"alert alert-warning\">You are using FREE version of karibuSMS
            <a href=\"#layout\" class=\"label label-info\" style=\"color: white; font-size:17px\" onclick=\"get_send({pg: 'payment', section: 'pay'}, 'content');\">Upgrade now</a></span>";
            } else {
                self::return_bundle_status($bp);
            }
        } else {
            self::$max_group = 1500000;
            self::$max_image = 100000;
            self::$max_contacts = 1000000;
            self::$max_sms = 10000;
            self::$max_video = 70000;

            self::$allow_social_page = 1;
            self::$allow_shortcode = 1;
            self::$allow_location = 1;
        }
    }

    private static function return_bundle_status($bundle_id) {
        $bundle_info = bundle::find_by_id($bundle_id);
        $bundle = array_shift($bundle_info);

        self::$max_group = $bundle->max_group;
        self::$max_image = $bundle->max_image;
        self::$max_contacts = $bundle->max_contacts;
        self::$max_sms = $bundle->max_sms;
        self::$max_video = $bundle->max_video;

        self::$allow_social_page = $bundle->allow_social_page;
        self::$allow_shortcode = $bundle->allow_shortcode;
        self::$allow_location = $bundle->allow_location;
        self::$allow_file_upload = $bundle->allow_file_upload;
    }

    private static function check_bundle() {
        global $ses_user;
        $bundle_info = bp::find_where(array('business_id' => $ses_user->id));
        if (!empty($bundle_info)) {
            $bundle = array_shift($bundle_info);
            return $bundle->bundle_id;
        } else {
            return 0;
        }
    }

    public static function is_try_period() {
        global $ses_user;
        global $db;
        if (!empty($ses_user)) {
            $db->query("select * from smart_start_offer where business_id = '" . $ses_user->id . "'");
            $row = $db->fetchArray();
            if (empty($row)) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

}
