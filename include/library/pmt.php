<?php

/**
  |--------------------------------------------------------------------------
  | Process payments in the system
  |--------------------------------------------------------------------------
  |
  | Payment processed include those of karibuSMS and karibuSMSpro done by M-pesa
  | and Tigo-pesa
  |
 */
class pmt {

    public $business_id;
    public $pmt_info;
    public $messaging_type=0;
    private $payment_id;
    private $paypal;
    private $bundles;
    private $bundle_amount;
    private $extra_amount;

    public function check_receipt($receipt) {
	$rps = rps::find_where(array("confirmation_code" => $receipt));
	$this->pmt_info = array_shift($rps);
	if ($this->pmt_info->ready == 1) {
	    echo '<div class="alert alert-danger"> This reference number is already processed</div>';
	    exit();
	}
	return empty($this->pmt_info) ? FALSE : TRUE;
    }

    public function paypal($paypal_data) {
	$this->paypal = (object) $paypal_data;
	return empty($this->paypal) ? FALSE : TRUE;
    }

    private function get_bundle() {
	$bundle = bundle::find_where(array('amount' => $this->bundle_amount));
	return array_shift($bundle);
    }

    private function create_bundle_pmt() {
	global $db;
	$data = array(
	    'payment_id' => $this->payment_id,
	    'business_id' => $this->business_id,
	    'bundle_id' => $this->get_bundle()->id);
	return $db->insert('bp', $data);
    }

    private function get_vendor() {
	return !empty($this->paypal) ? 'paypal' : $this->pmt_info->vendor;
    }

    private function get_verified() {
	global $ses_user;
	return empty($ses_user) ? 'android_app' : 'web_based';
    }

    private function get_verification_code() {
	return empty($this->pmt_info) ? '' : $this->pmt_info->confirmation_code;
    }

    private function get_amount() {
	$extra_amount = payment_extra::find_where(array('business_id' => $this->business_id, 'processed' => 1));
	if (!empty($extra_amount)) {
	    $amount = empty($this->pmt_info) ? $this->paypal->mc_gross + $extra_amount : $this->pmt_info->amount + $extra_amount;
	} else {
	    $amount = empty($this->pmt_info) ? $this->paypal->mc_gross : $this->pmt_info->amount;
	}
	return $amount;
    }

    private function find_amount() {
	$this->bundles = bundle::find_all();
	$amount_array = array();
	//we need to put bundle id in a key for easy reference
	if (empty($this->paypal)) {
	    foreach ($this->bundles as $bundle) {
		$amount_array[] = $bundle->amount; //we put Tsh amount into an array
	    }
	} else {
	    foreach ($this->bundles as $bundle) {
		$amount_array[] = $bundle->amount_usd; //we put usdollar amount into an array
	    }
	}
	//we sort an array to be in ascending order
	sort($amount_array);
	//loop to an array
	$que = array();
	foreach ($amount_array as $amount) {
	    //find the difference btn array
	    $diff = $amount - $this->get_amount();
	    if ($diff <= 0) {
		//difference is -ve, store the value in the que
		$que[] = $diff;
	    }
	}
	rsort($que); //we sort que into ascending order
	if (empty($que)) {
	    //difference is positive, this means amount is small, so we just store it 
	    $this->extra_amount = $this->get_amount();
	    $this->save_extra_payment();
	} elseif ($que[0] == 0) {
	    //the amount is exactly in the bundle
	    $this->bundle_amount = $this->get_amount();
	} else {
	    //the amount is exactly in the bundle	    
	    $this->extra_amount = abs($que[0]);
	    $this->bundle_amount = $this->get_amount() - $this->extra_amount;
	    $this->save_extra_payment();
	}
	return TRUE;
    }

    private function save_extra_payment() {
	global $db;
	$data = array(
	    'business_id' => $this->business_id,
	    'amount' => $this->extra_amount,
	    'currency' => $this->get_currency()
	);
	return $db->insert('payment_extra', $data);
    }

    private function update_rps() {
	global $db;
	$db->update('rps', array('ready' => 1), "id='" . $this->get_rps_id() . "'");
    }

    private function get_currency() {
	return empty($this->pmt_info) ? $this->paypal->mc_currency : 'Tsh';
    }

    private function get_rps_id() {
	return empty($this->pmt_info) ? '' : $this->pmt_info->id;
    }

    public function make() {
	global $db;
	$this->find_amount();
	$py_data = array(
	    'business_id' => $this->business_id,
	    'pl_id' => empty($this->paypal) ? '' : $this->paypal->id,
	    'verification_code' => $this->get_verification_code(),
	    'rps_id' => $this->get_rps_id(),
	    'method' => $this->get_vendor(),
	    'verified_by' => $this->get_verified(),
	    'currency' => $this->get_currency(),
	    'amount' => $this->bundle_amount,
	    'messaging_type'=>  $this->messaging_type,
	    'processed' => 1
	);
	if ($db->insert('payment', $py_data) == 1) {
	    $this->payment_id = $db->id();
	    $this->update_rps();
	}
	return $this->create_bundle_pmt();
    }

    public function make_per_sms() {
	global $db;
	$amount = empty($this->pmt_info) ? $this->paypal->mc_gross : $this->pmt_info->amount;
	$py_data = array(
	    'business_id' => $this->business_id,
	    'pl_id' => empty($this->paypal) ? '' : $this->paypal->id,
	    'verification_code' => $this->get_verification_code(),
	    'rps_id' => $this->get_rps_id(),
	    'method' => $this->get_vendor(),
	    'verified_by' => $this->get_verified(),
	    'currency' => $this->get_currency(),
	    'amount' => $amount,
	    'per_sms'=>1,
	    'messaging_type'=>1,
	    'processed' => 1
	);
	if ($db->insert('payment', $py_data) == 1) {
	    $this->payment_id = $db->id();
	    $this->bsextrsms();
	    $this->update_rps();
	}
	return TRUE;
    }

    private function bsextrsms() {
	global $db;
	$amount = empty($this->pmt_info) ? $this->paypal->mc_gross : $this->pmt_info->amount;
	$no_of_sms=  ceil($amount/COST_PER_SMS);
	$data = array(
	    'total_sms' => $no_of_sms,
	    'business_id' => $this->business_id,
	    'amount_paid' => $amount);
	return $db->insert('bsextrsms', $data);
    }
}
